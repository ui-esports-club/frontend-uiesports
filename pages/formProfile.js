import {useRouter} from "next/router";
import { useEffect, useState, useRef } from "react";
import { Alert, Snackbar } from "@mui/material";

function FormProfile(props) {
    const [token, setToken] = useState(false);
    const [list, setList] = useState([]);
    const [imagePreview, setPreview] = useState();
    const [fileChanged, setFileChanged] = useState();
    const [isSubmitting, setSubmitting] = useState(false);

    // State Alert
    const [successAlert, setSucessAlert] = useState(false);
    const [fileSizeAlert, setFileSizeAlert] = useState(false);
    const [whatsappAlert, setWhatsappAlert] = useState(false);

    const router = useRouter();

    // Ref for form
    const username = useRef(null);
    const profileTag = useRef(null);
    const npm = useRef(null);
    const faculty = useRef(null);
    const generation = useRef(null);
    const line = useRef(null);
    const whatsapp = useRef(null);
    const email = useRef(null);
    const photo = useRef(null);

    useEffect(()=>{
        if(photo.current.files[0]){
            if(photo.current.files[0].size <= 2000000){
                const objectUrl = URL.createObjectURL(photo.current.files[0]);
                setPreview(objectUrl);
            }
            else{
                setFileSizeAlert(true);
            }

        };
    },[fileChanged]);

    useEffect(() => {
        fetch('/api/get-auth', {
            method: 'POST'})
          .then((res) => res.json())
          .then((data) => {
            if(!data.message){
                setToken(data.access_token)
            }else{
                router.push('/auth/error')
            }

        })
    },[])

    useEffect(() => {
        if(token){
            let mounted = true;
            fetch(`${process.env.NEXT_PUBLIC_BACKEND_SERVER}/profile/`, {
                method: 'GET',
                headers : {
                    'Content-Type' : 'application/json',
                    'Authorization' : `Bearer ${token}`
                }
            })
            .then(data => data.json())
            .then(items => {
                if(mounted) {
                    setPreview(items.photo);
                    setList(items)
                }
            })
            return () => mounted = false;
        }
    }, [token])

    async function putData() {
        setSubmitting(true);
        let putData = new FormData();
        if (photo.current.files[0]) {
            putData.append('photo', photo.current.files[0])
        }
        if (whatsapp.current.value.length <= 12 && whatsapp.current.value.length >= 10 && whatsapp.current.value.slice(0,2)=="08"){
            putData.append('whatsapp', whatsapp.current.value);
        }else{
            setWhatsappAlert(true);
            setSubmitting(false);
            return;
        }
        putData.append('tag', profileTag.current.value);
        putData.append('id_line', line.current.value);
        putData.append('email', email.current.value);
        const res = await fetch(`/api/form-profile`, {
            method : 'PATCH',
            headers : {
                'Authorization' : `Bearer ${token}`
            },
            body : putData,
        })
        if (!res.ok) {
            alert('Data tidak berhasil dimasukkan')
        }else{
            setSucessAlert(true);
            router.push('/formProfile');
        }
        setSubmitting(false);
    }

    return (
        <div className="profile-container">
            <div className="profile-container flex justify-center">
                <div className="photo-container">
                    <img src = {imagePreview} className="h-48 w-48 object-cover"></img>
                    <label for="fileInput">
                        Change Images
                        <input id="fileInput" onChange={setFileChanged} type="file"  ref = {photo} className="block w-full text-sm text-white-500 file:mr-4 file:py-2 file:px-4 file:border-1 file:text-sm file:font-medium file:text-blue-700 hover:file:bg-blue-100" required/>
                    </label>
                </div>

                <div className ="form-container">
                    <h1 className="text-white1 text-2xl font-bold pb-3">Edit Profile</h1>
                    <div className="items-center border-b border-gray2 py-2">
                        <p className="text-lg text-gray2">Username</p>
                        <input id ="username" ref = {username} className="appearance-none bg-transparent border-none w-full text-gray2 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" placeholder="Insert Your Username" aria-label="Full name" defaultValue={list.username} disabled />
                    </div>
                    <div className="items-center border-b border-white-500 py-2 pt-8">
                        <p className="text-lg text-white1">Profile Tag</p>
                        <input id = "profileTag" ref = {profileTag} className="appearance-none bg-transparent border-none w-full text-white1 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" placeholder="Insert Your Current Tag" defaultValue = {list.tag} aria-label="Full name" required/>
                    </div>
                    <div className="items-center border-b border-gray2 py-2  pt-8">
                        <p className="text-lg text-gray2">NPM</p>
                        <input id = "npm" ref = {npm} className="appearance-none bg-transparent border-none w-full text-gray2 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" aria-label="Full name" defaultValue={list.npm}disabled />
                    </div>
                    <div className="items-center border-b border-gray2 py-2  pt-8">
                        <p className="text-lg text-gray2">Faculty</p>
                        <input id = "faculty" ref = {faculty} className="appearance-none bg-transparent border-none w-full text-gray2 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" aria-label="Full name" defaultValue= {list.fakultas} disabled />
                    </div>
                    <div className="items-center border-b border-gray2 py-2  pt-8">
                        <p className="text-lg text-gray2">Generation</p>
                        <input id = "generation" ref = {generation} className="appearance-none bg-transparent border-none w-full text-gray2 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" aria-label="Full name" defaultValue= {list.angkatan} disabled />
                    </div>
                    <h1 className="text-white text-2xl font-bold pb-3 pt-5">Contacts</h1>
                    <table className="table-auto">
                        <tbody>
                            <tr>
                                <td className="px-1 py-2">
                                    <p className="text-lg text-white1 pr-5">Line</p>
                                </td>
                                <td className="px-1 py-2">
                                    <input id = "line" ref = {line} className="appearance-none bg-transparent border-b border-white-500  w-full text-white1 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" placeholder="Insert Your Line ID"  defaultValue= {list.id_line} aria-label="Full name"/>
                                </td>
                            </tr>
                            <tr className="bg-gray-100">
                                <td className="px-1 py-2">
                                    <p className="text-lg text-white1 pr-5">Whatsapp</p>
                                </td>
                                <td className="px-1 py-2">
                                    <input id = "whatsapp" ref = {whatsapp} className="appearance-none bg-transparent border-b border-white-500  w-full text-white1 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" placeholder="Insert Your WhatsApp Number"  defaultValue= {list.whatsapp}  aria-label="Full name"/>
                                </td>
                            </tr>
                            <tr>
                                <td className="px-1 py-2">
                                    <p className="text-lg text-white1 pr-5">Email</p>
                                </td>
                                <td className="px-1 py-2">
                                    <input id = "email" ref = {email} className="appearance-none bg-transparent border-b border-white-500  w-full text-white1 mr-3 py-2 px-2 leading-tight focus:outline-none" type="email" placeholder="email@example.com"  defaultValue= {list.email} aria-label="Full name"/>
                                </td>
                            </tr>
                            {/* <tr>
                            <td className="px-1 py-2">
                                <p className="text-lg text-white1 pr-5">Account Info</p>
                            </td>
                            <td className="px-1 py-2">
                            <input id = "info"  className="appearance-none bg-transparent border-b border-white-500  w-full text-white1 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" placeholder="" defaultValue= {list.tag} aria-label="Full name"/>
                            </td>
                            </tr>
                            <tr>
                            <td className="px-1 py-2">
                                <p className="text-lg text-white1 pr-5">Nickname</p>
                            </td>
                            <td className="px-1 py-2">
                            <input id = "nickname" ref = {nickname} className="appearance-none bg-transparent border-b border-white-500  w-full text-white1 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" placeholder="Your Nickname"  defaultValue={list.first_name} aria-label="Full name"/>
                            </td>
                            </tr>
                            <tr>
                            <td className="px-1 py-2">
                                <p className="text-lg text-white1 pr-5">ID</p>
                            </td>
                            <td className="px-1 py-2">
                            <input id = "id" ref = {instagram} className="appearance-none bg-transparent border-b border-white-500  w-full text-white1 mr-3 py-2 px-2 leading-tight focus:outline-none" type="text" placeholder="Your ID"  defaultValue={list.instagram} aria-label="Full name"/>
                            </td>
                            </tr> */}
                        </tbody>
                    </table>
                    <button type = "submit" onClick = {putData} className="bg-purple text-white1 text-lg font-medium py-3 px-5 w-full my-5">
                    Save Changes 
                    </button>

                    {isSubmitting && 
                        <div className="spinner">
                            <img src="https://storage.googleapis.com/uiesports/assets/spinner.svg"></img>
                        </div>
                    }
                </div>
            </div> 

            <Snackbar
                open={successAlert}
                autoHideDuration={2000}
                onClose={()=>{setSucessAlert(false)}}
            >
                <Alert severity="success" variant="filled">
                    Your Data has been changed.
                </Alert>
            </Snackbar>

            <Snackbar
                open={fileSizeAlert}
                autoHideDuration={2000}
                onClose={()=>{setFileSizeAlert(false)}}
            >
                <Alert severity="error" variant="filled">
                    Your File Must be 2 MB Maximum.
                </Alert>
            </Snackbar>
            <Snackbar
                open={whatsappAlert}
                autoHideDuration={2000}
                onClose={()=>{setWhatsappAlert(false)}}
            >
                <Alert severity="error" variant="filled">
                    Please Enter the Valid Whatsapp Number.
                </Alert>
            </Snackbar>
            <div className="footer-transition">
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
            </div>
        </div>
    )
}

export default FormProfile;
