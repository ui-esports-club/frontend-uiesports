import Layout from "../../components/layout"
import Header from '../../components/Database/Header';
import Main from "./Main"

const DatabasePage = (props) => {
    return (
        <>
            <Header />
            <Main />
        </>
    )
}


DatabasePage.getLayout = (page) => (
    <Layout active="Database">
        {page}
    </Layout>
)


export default DatabasePage
