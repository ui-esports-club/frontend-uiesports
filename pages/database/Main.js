// import MemberBox from "./MemberBox"
import React, {useState} from "react"
import {Pagination} from "@mui/material"
// import Modal from "./Modal"
// import SearchBar from "./SearchBar"
// import MemberFilter from "./MemberFilter"
import {useRouter} from "next/router";
import Modal from '../../components/Database/Modal';
import SearchBar from '../../components/Database/SearchBar';
import MemberFilter from '../../components/Database/MemberFilter';
import MemberBox from '../../components/Database/MemberBox';



const Main = () => {
    const router = useRouter();
    const [token, setToken] = useState(false);

    React.useEffect(() => {
        fetch('/api/get-auth', {
            method: 'POST'})
            .then((res) => res.json())
            .then((data) => {
                if(!data.message){
                    setToken(data.access_token)
                }else{
                    router.push('/auth/error')
                }

            })
    },[])

    const [modalDataState, setModalDataState] = useState({
        username: "Undefined",
        first_name:'Undefined',
        last_name:'Undefined',
        fakultas: "Undefined",
        imageSrc: "/database/empty-profile-picture.jpg",
        npm: "Undefined",
        angkatan: "Undefined",
        divisi: [],
        id_line: "Undefined",
        instagram: "Undefined",
        email: "Undefined",
    })

    const [isLoadingState, setIsLoadingState] = useState(false)

    const [numberOfPages, setNumberOfPages] = useState(0)


    const [memberListState, setMemberListState] = useState([]);

    const [showModalState, setShowModalState] = useState(false)

    const [loadErrorState, setLoadErrorState] = useState(false)

    const [page, setPage] = useState(1)

    const [searchQuery, setSearchQuery] = useState('')

    const [divisi, setDivisi] = useState('')

    const handlePaginationChange = async (event, value) => {
        setPage(value)
        setIsLoadingState(true)
    }

    const handleFilterChange = async (event) => {
        setDivisi(event.target.value)
        setIsLoadingState(true)
    }

    const handleSearchFilterChange = async (event) => {
        setSearchQuery(event.target.value)
        setIsLoadingState(true)
    }


    React.useEffect( () => {
        if(token) {
            let innerUrl = '' + page

            if (searchQuery !== '') {
                innerUrl = `${innerUrl}&search=${searchQuery}`
            }

            if (divisi !== 'Any') {
                innerUrl = `${innerUrl}&divisi__nama_divisi=${divisi}`
            }

            fetch(`https://backend-uiesports-staging.herokuapp.com/database/all?page=${innerUrl}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            }).then(async res => {
                if (res.ok) {
                    setIsLoadingState(false)
                    const data = await res.json()
                    setNumberOfPages(Math.ceil(data.count / 8))
                    setMemberListState(data.results)
                }
            })

        }


    }, [divisi, page, searchQuery, token]);


    return (
        <>
            <Modal 
                show={showModalState}
                member={modalDataState}
                handleClose={() => setShowModalState(false)}
            />
            <div className="bg-[#1E2021] min-h-[500px]">
                <SearchBar
                    searchFilter={searchQuery}
                    changeSearchFilterFunction={handleSearchFilterChange}
                />
                <MemberFilter 
                    activeFilter={divisi}
                    changeFilterFunction={handleFilterChange}
                />
                <div className="grid sm:grid-cols-1 md:grid-cols-2 grid-cols-4 gap-20 sm:p-4 p-20 place-items-stretch">
                    { 
                    isLoadingState ?
                    <>
                        <MemberBox skeleton />
                        <MemberBox skeleton />
                        <MemberBox skeleton />
                        <MemberBox skeleton />
                        <MemberBox skeleton />
                        <MemberBox skeleton />
                        <MemberBox skeleton />
                        <MemberBox skeleton />
                    </>
                    :
                    loadErrorState ?
                    "Error loading, the server might be busy, please try again"
                    :
                    memberListState.map((member) => (
                        <MemberBox
                            tag={member.tag}
                            key={member.username}
                            username={member.username}
                            first_name={member.first_name}
                            last_name={member.last_name}
                            divisi={member.divisi}
                            photo={member.photo}
                            onClickFunction={() => {
                                setModalDataState(member)
                                setShowModalState(true)
                            }}
                        />
                    ))
                    }
                </div>
                <div className="flex justify-center py-10" id="pagination">
                    <Pagination 
                        count={numberOfPages}
                        page={page} 
                        shape="rounded" 
                        color="secondary" 
                        size="large"
                        onChange={handlePaginationChange}
                        disabled={isLoadingState}
                    />
                </div>
            </div>
        </>
    )
}

export default Main