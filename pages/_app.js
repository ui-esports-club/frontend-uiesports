import '../styles/globals.css';
import "../styles/auth.css";
import '../styles/Carousel.css';
import "../styles/card.css";
import '../styles/ourteams.css';
import '../styles/profile.css';
import '../styles/navbar.css';
import '../styles/footer.css';
import '../styles/Home.module.css';
import '../styles/alumni.css';
import '../styles/formProfile.css';
import '../styles/database/pagination.css';
import '../styles/database/filter-dropdown.css';
import '../styles/index.css';
import '../styles/formProfile.css';
import '../styles/customError.css';
import '../styles/navbar-mobile.css';
import '../styles/changePassword.css';
import '../styles/teaserJersey.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from '../components/layout';
import {AnimatePresence} from 'framer-motion';
import Head from 'next/head';

function MyApp({ Component, pageProps }) {
  return (
    <AnimatePresence exitBeforeEnter>
      <Layout>
        <Head>
          <link rel='icon' type='image/x-icon' href="/favicon.png" />
          <title>UI Esports Club</title>
        </Head>
        <Component {...pageProps} />
      </Layout>
    </AnimatePresence>
    )
}

export default MyApp
