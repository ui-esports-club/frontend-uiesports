import ProductCard from "../components/productShowcase/product-card"
import ProductHeader from "../components/productShowcase/product-header"

export default function ShowcaseProduct() {

    return(
        <div className="productshowcase-page">


            <ProductHeader/>
            <div className="productshowcase-section-1">
                <ProductCard bgColor='pink' />
                <ProductCard bgColor='green'/>
                <ProductCard bgColor='purple'/>
                <ProductCard bgColor='blue'/>
            </div>

            <div className="footer-transition">
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
            </div>
        </div>
    )

}