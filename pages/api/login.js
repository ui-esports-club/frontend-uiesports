import cookie from "cookie";
import { checkProfileUncomplete } from "../../functions/sessionAuth";

export default async function login(req, res) {

    if (req.method === 'POST') {
        const username = req.body.username
        const password = req.body.password
        const rememberme = req.body.rememberme

        if (username && password && (rememberme !== null)) {

            let response = await fetch("http://esports.ui.ac.id:8000/auth/jwt/token", {
                method: 'POST',
                body: JSON.stringify({
                    username,
                    password,
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            if (response.ok) {
                const data = await response.json()
                const profile_uncompleted = await checkProfileUncomplete(data.access)
                
                const session_non_http = {path: '/',sameSite:'lax'}
                const one_day  = {httpOnly: true,path: '/',sameSite:'lax',maxAge: 3600 * 24}
                const one_month= {httpOnly: true,path: '/',sameSite:'lax',maxAge: 3600 * 24 * 30}
                if (rememberme) {
                    res.setHeader('Set-Cookie', [
                        cookie.serialize("access_token", data.access, one_day),
                        cookie.serialize("refresh_token", data.refresh, one_month),
                        cookie.serialize("role", data.role, one_month),
                        cookie.serialize("profile_uncompleted", profile_uncompleted,session_non_http),
                    ])
                }
                else {
                    res.setHeader('Set-Cookie', [
                        cookie.serialize("access_token", data.access, one_day),
                        cookie.serialize("role", data.role, one_day),
                        cookie.serialize("profile_uncompleted", profile_uncompleted,session_non_http),
                    ])
                }
                return res.status(200).json({ message: `Login Success `})

            }
            else {
                return res.status(response.status).json({ message: "Login Failed" })
            }
        }
        else {
            return res.status(400).json({ message: "Parameter Uncompleted" })
        }
    }
    else {
        return res.status(400).json({ message: `Method ${req.method} Not Allowed` })
    }

};