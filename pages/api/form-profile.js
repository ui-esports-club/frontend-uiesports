import cookie from "cookie";
import { checkProfileUncomplete } from "../../functions/sessionAuth";
import formidable from "formidable";
import fs from "fs";

export default async function formProfile (req,res){
    if(req.method === 'GET'){
        let response = await fetch(`http://esports.ui.ac.id:8000/profile/`, {
            method: 'GET',
            headers : {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${req.cookies.access_token}`
            }
        })

        if(response.ok){
            let data = await response.json();
            return res.status(200).json(data);
        }
    }

    if(req.method === 'PATCH'){
        let putData = new FormData();
        const form = formidable({});
        form.parse(req, async function (err, fields, files) {
            if (files.photo.length > 0){
                putData.append('photo', files.photo[0]);
            };
            putData.append('whatsapp', fields.whatsapp);
            putData.append('tag', fields.tag);
            putData.append('id_line', fields.id_line);
            putData.append('email', fields.email);
        });

        const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_SERVER}/profile/`, {
            method : 'PATCH',
            headers : {
                'Authorization' : `Bearer ${req.cookies.access_token}`
            },
            body : putData,
        });

        console.log(response);
    }
}

export const config = {
    api: {
      bodyParser: false, // Disallow body parsing, consume as stream
    },
  };