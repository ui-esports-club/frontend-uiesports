import cookie from "cookie";

export default async function logout(req, res) {
    const cookie_set = { httpOnly: true, path: '/', expires: new Date(0)}
    res.setHeader('Set-Cookie', [
        cookie.serialize("access_token", "", cookie_set),
        cookie.serialize("refresh_token", "", cookie_set),
        cookie.serialize("role", "", cookie_set),
        cookie.serialize("profile_uncompleted", "", cookie_set),
    ])
    res.statusCode = 200
    res.json({message: "Logout success"})
}