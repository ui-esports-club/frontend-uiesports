export default async function login(req, res) {


        const referer = req.headers.referer ? req.headers.referer : 'unidentified'
        if(
            referer.startsWith('http://localhost:3000') ||                           //For Local Development
            referer.startsWith('http://frontend-uiesports-staging.herokuapp.com') || //For Staging Deployment
            referer.startsWith('https://uiesports.netlify.app')|| //For Staging Deployment
            referer.startsWith('http://frontend-uiesports.netlify.app')  ||          //For Production Deplyment
            referer.startsWith('https://frontend-uiesports.netlify.app') ||        //For Production Deplyment
            referer.startsWith('http://esports.ui.ac.id') ||
            referer.startsWith('https://esports.ui.ac.id')
        ){
            const access_token = req.cookies.access_token ? req.cookies.access_token : false
            const refresh_token = req.cookies.refresh_token ? req.cookies.refresh_token : false
            const role = req.cookies.role ? req.cookies.role : false
    
            return res.json({
                access_token,
                refresh_token,
                role,
                referer
            })
        }
        else{
            return res.status(403).json({ message: `Forbidden Access ${referer}`,referer })
        }

};
