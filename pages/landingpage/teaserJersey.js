import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Scrollbar,  Autoplay} from "swiper";
import "swiper/css";
import "swiper/css/pagination";

function teaserJersey(props){
    SwiperCore.use([Autoplay]);

    return(
        <div className="teaserJerseyContainer">

            <div className="headerTeaser">
                <img src={"https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/header-jersey.webp"} className="headerPhoto"/>
                <img src={"https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/header-jersey-mobile.webp"} className="headerPhotoMobile"/>
                <img src={"https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/background-header-desktop.webp"} className="headerBackgroundDesktop"/>
                <img src={"https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/background-header-mobile.webp"} className="headerBackgroundMobile"/>
                <div className="headerTitle">
                    UIESPORTS<br/>
                    <span>
                        JERSEY<br/>
                        MERCHANDISE
                    </span>
                    <br/>
                    <a href="/">Order Now!</a>
                </div>
            </div>

            <div className="aboutSection">
                <h3 className="title">
                    About Jersey UI Esports
                </h3>
                <p className="content">
                UI Esports Jersey is an identity for the Organization as it design represent UI Esports Identity. The white and pink color represents the transparancy and joy in the Organization ecosystem. The Organization in the chest combined with the University logo will remind that UI Esports Club is under the shade of Universitas Indonesia. It is hoped to be the identity of the Organization.
                </p>
            </div>

            <div className="featureSection">
                <div className="bestQuality">
                    <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/name-customization.webp" className="thumbnail-1"/>
                    <div className="desc">
                        <h3>
                            Best Quality
                        </h3>
                        <p>
                            Our jersey made by the best cotton material that will absorb heat and sweat maximally. The screen printed logo will have more durability and will not disintegrate easily. The jersey finished with neat stitching by the best taylor to keep the quality of the end product. And finally, the strong color dyed  using the best dye quality will stay longer and make you stand out when you wear it.
                        </p>
                    </div>
                </div>
                <div className="nameCustomization">
                    <div className="desc">
                        <h3>
                            Name Customization
                        </h3>
                        <p>
                        To increase the personalization element in the jersey, The Creative Business teams enable user to customize and personalize the jersey so they can felt more connected to the jersey itself by creating name customization in the back of the jersey. User can create their own nickname using any words and any symbol as far as their imaginations. User also can choose to not customize it if they prefer to not put it.
                        </p>
                    </div>
                    <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/best-quality.webp" className="thumbnail-1"/>
                </div>
            </div>

            <div className='catalogSection'>
                <Swiper
                    speed={500}
                    direction="horizontal"
                    autoplay={{delay: 3000}}
                    slidesPerView="auto"
                    spaceBetween={40}
                    centeredSlides={true}
                    loop={true}
                    scrollbar={{
                        hide: true,
                        }}
                    modules={[Scrollbar]}
                    className="mySwiper"
                    breakpoints= {{

                        640: {
                            slidesPerView: 1.75,
                        },
                        768: {
                            slidesPerView: 2.75,
                        },
                        1080: {
                            slidesPerView: 3.25,
                        },
                    }}
                >
                    <SwiperSlide >
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App1.webp" />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App10.webp"/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App2.webp"/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App3.webp"/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App4.webp"/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App5.webp"/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App6.webp"/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App7.webp"/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App8.webp"/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/Jersey-App9.webp"/>
                    </SwiperSlide>
                </Swiper>
            </div>

            <div className="orderSection">
                <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/order-desktop.webp" className="desktopOrder"/>
                <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/order-mobile.webp" className="mobileOrder"/>
            </div>

            <div className="comingSoonSection">
                <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/cs-desktop.webp" className="desktopFooter"/>
                <img src="https://storage.googleapis.com/uiesports/assets/landing-page/teaser-jersey/cs-mobile.webp" className="mobileFooter"/>
            </div>

            <div className="footer-transition">
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
            </div>
        </div>
    )
}

export default teaserJersey;