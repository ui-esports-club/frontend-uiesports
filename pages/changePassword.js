import {useRouter} from "next/router";
import { useEffect, useState, useRef } from "react";
import { Alert, Snackbar } from "@mui/material";

function ChangePassword(props) {
    const [token, setToken] = useState(false);
    const [list, setList] = useState([]);
    const [isSubmitting, setSubmitting] = useState(false);

    // State Alert
    const [successAlert, setSucessAlert] = useState(false);
    const [oldPasswordAlert, setoldPasswordAlert] = useState(false);
    const [confirmWrongAlert, setConfirmWrongAlert] = useState(false);
    const [samePasswordAlert, setSamePasswordAlert] = useState(false);

    const router = useRouter();

    // Ref for form
    const old_password = useRef(null);
    const new_password = useRef(null);
    const confirm_password = useRef(null);

    useEffect(() => {
        fetch('/api/get-auth', {
            method: 'POST'})
          .then((res) => res.json())
          .then((data) => {
            if(!data.message){
                setToken(data.access_token)
            }else{
                router.push('/auth/error')
            }

        })
    },[]);

    function viewPassword(elementCount) {
        var passwordInput = document.getElementById(`password-${elementCount}`);
        var hideIcon = document.getElementById(`password-profile-hide-${elementCount}`);
        var showIcon = document.getElementById(`password-profile-show-${elementCount}`);
        if (passwordInput.type === "password"){
            passwordInput.type = "text";
            hideIcon.style.display = "none";
            showIcon.style.display = "initial";
            
        }else{
            passwordInput.type = "password";
            hideIcon.style.display = "initial";
            showIcon.style.display = "none";
        }
    };

    async function verifyOldPassword() {
        let putData = new FormData();

        putData.append('old_password', old_password.current.value);

        const res = await fetch("http://esports.ui.ac.id:8000/profile/check-password", {
            method: 'POST',
            headers : {
                'Authorization' : `Bearer ${token}`
            },
            body : putData,
        }).then((res) => res.json());

        return await res;
    };
    

    async function putData() {
        setSubmitting(true);

        let putData = new FormData();
        let response = await verifyOldPassword();

        if(response.message === "Password correct"){
            if(new_password.current.value === confirm_password.current.value){
                if(new_password.current.value != old_password.current.value){
                    putData.append('password', new_password.current.value)
                    const res = await fetch("http://esports.ui.ac.id:8000/profile/", {
                        method : 'PATCH',
                        headers : {
                            'Authorization' : `Bearer ${token}`
                        },
                        body : putData,
                    })
                    if (!res.ok) {
                        alert('Data tidak berhasil dimasukkan')
                    }else{
                        setSucessAlert(true);
                        router.push('/');
                    }
                }else{
                    // Password lama dan baru sama
                    setSamePasswordAlert(true);
                }

            }else{
                // Password dan Confirm Password beda
                setConfirmWrongAlert(true);
            }
        }else{
            // Old Password salah
            setoldPasswordAlert(true);
        }
        setSubmitting(false);
    };

    return (
        <div className="changepassword-container">
            <div className="profile-container flex justify-center">
                <div className ="form-container">
                    <h1 className="text-white1 text-2xl font-bold pb-3">Change Password</h1>
                    <div className="items-center border-b border-white-500 py-2">
                        <p className="text-lg text-white pt-3">Old Password</p>
                        <div className="d-flex">
                            <input id ="password-1" ref = {old_password} className="appearance-none bg-transparent border-none w-full text-white mr-3 py-2 px-2 leading-tight focus:outline-none" type="password" placeholder="Insert Your Password" aria-label="Password" defaultValue={list.password} required/>
                            <input id="passwordRadio-1" type="checkbox" onClick={() => viewPassword(1)}/>
                            <label for="passwordRadio-1" class="passwordLabel">
                                <img id="password-profile-hide-1" class="password-radio-icon" src="https://storage.googleapis.com/uiesports/assets/icon/hide.webp"></img>
                                <img id="password-profile-show-1" class="password-radio-icon" src="https://storage.googleapis.com/uiesports/assets/icon/show.webp"></img>
                            </label>
                        </div>
                    </div>
                    <div className="items-center border-b border-white-500 py-2">
                        <p className="text-lg text-white pt-3">New Password</p>
                        <div className="d-flex">
                            <input id ="password-2" ref = {new_password} className="appearance-none bg-transparent border-none w-full text-white mr-3 py-2 px-2 leading-tight focus:outline-none" type="password" placeholder="Insert Your Password" aria-label="Password" defaultValue={list.password} required/>
                            <input id="passwordRadio-2" type="checkbox" onClick={() => viewPassword(2)}/>
                            <label for="passwordRadio-2" class="passwordLabel">
                                <img id="password-profile-hide-2" class="password-radio-icon" src="https://storage.googleapis.com/uiesports/assets/icon/hide.webp"></img>
                                <img id="password-profile-show-2" class="password-radio-icon" src="https://storage.googleapis.com/uiesports/assets/icon/show.webp"></img>
                            </label>
                        </div>
                    </div>
                    <div className="items-center border-b border-white-500 py-2">
                        <p className="text-lg text-white pt-3">Confirm New Password</p>
                        <div className="d-flex">
                            <input id ="password-3" ref = {confirm_password} className="appearance-none bg-transparent border-none w-full text-white mr-3 py-2 px-2 leading-tight focus:outline-none" type="password" placeholder="Insert Your Password" aria-label="Password" defaultValue={list.password} required/>
                            <input id="passwordRadio-3" type="checkbox" onClick={() => viewPassword(3)}/>
                            <label for="passwordRadio-3" class="passwordLabel">
                                <img id="password-profile-hide-3" class="password-radio-icon" src="https://storage.googleapis.com/uiesports/assets/icon/hide.webp"></img>
                                <img id="password-profile-show-3" class="password-radio-icon" src="https://storage.googleapis.com/uiesports/assets/icon/show.webp"></img>
                            </label>
                        </div>
                    </div>
                    <button type = "submit" onClick = {putData} className="bg-purple text-white1 text-lg font-medium py-3 px-5 w-full my-5">
                    Save Changes 
                    </button>

                    {isSubmitting && 
                        <div className="spinner">
                            <img src="https://storage.googleapis.com/uiesports/assets/spinner.svg"></img>
                        </div>
                    }
                </div>
            </div> 

            <Snackbar
                open={successAlert}
                autoHideDuration={2000}
                onClose={()=>{setSucessAlert(false)}}
            >
                <Alert severity="success" variant="filled">
                    Your Data has been changed.
                </Alert>
            </Snackbar>

            <Snackbar
                open={oldPasswordAlert}
                autoHideDuration={2000}
                onClose={()=>{setoldPasswordAlert(false)}}
            >
                <Alert severity="error" variant="filled">
                    Your Old Password is not Correct.
                </Alert>
            </Snackbar>
            <Snackbar
                open={confirmWrongAlert}
                autoHideDuration={2000}
                onClose={()=>{setConfirmWrongAlert(false)}}
            >
                <Alert severity="error" variant="filled">
                    Your Confirmation Password is different than New Password.
                </Alert>
            </Snackbar>
            <Snackbar
                open={samePasswordAlert}
                autoHideDuration={2000}
                onClose={()=>{setSamePasswordAlert(false)}}
            >
                <Alert severity="error" variant="filled">
                    New Password Must be Different than Old Password.
                </Alert>
            </Snackbar>
            <div className="footer-transition">
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
            </div>
        </div>
    )
}

export default ChangePassword;
