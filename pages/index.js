import React, {useEffect, useState} from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import ValorantCard from '../components/ValorantCard.js';
import Dota2Card from '../components/Dota2Card.js';
import PubgmCard from '../components/PubgmCard.js';
import MlCard from '../components/MlCard.js';
import ProfileCard from '../components/profileCard.js';
import {motion} from 'framer-motion';
import Head from 'next/head';
import Cookies from 'js-cookie';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';

export default function Index() {
    const profile_uncompleted = Cookies.get('profile_uncompleted');
    const [modalShow, setModal] = useState(false);


    useEffect(() => {
      if(profile_uncompleted == 'true'){
        Cookies.remove('profile_uncompleted');
        setModal(true);
      }
    },[]);
    return (
      <div>
        <Head>
            <title>UI Esports Club</title>
        </Head>
        <div className="landingpage-content">
        <Carousel>
          <div className="imageCarousel">
            <img src="https://storage.googleapis.com/uiesports/assets/landing-page/Thumbnail%20Carousel_3.webp" alt="image1" className="desktop-image"/>
            <img src="https://storage.googleapis.com/uiesports/assets/landing-page/mobile-carousel2.webp" alt="image3" className="mobile-image"/>
            <div className="overlay1"></div>
            <div className="overlay2"></div>
            <motion.div 
              className="textOverlay"
              initial={{
                y: '3em',
                opacity: -1,
              }}
              animate={{
                y:0,
                opacity: 1
              }}
              transition={{
                duration: .5,
                ease: [0.43, 0.13, 0.23, 0.96]
              }}  
            >
              <h1 className="head head1">UPDATE</h1>
              <h3 className="subhead">UI Esports Website First Launching</h3>
              <p className="plainText">
              Tahun ini, UI Esports memiliki program kerja website yang bertujuan untuk dapat mempererat komunitas dan mempermudah orang-orang non UKM untuk mendapatkan informasi mengenai UI Esports. Website ini dibuat dengan cara mengkolaborasikan pengurus UI Esports sehingga menghasilkan produk yang maksimal.
              </p>
            </motion.div>
          </div>
          <div className="imageCarousel">
            <img src="https://storage.googleapis.com/uiesports/assets/landing-page/Thumbnail%20Carousel_2.webp" alt="image2" className="desktop-image"/>
            <img src="https://storage.googleapis.com/uiesports/assets/landing-page/mobile-carousel3.webp" alt="image3" className="mobile-image"/>
            <div className="overlay1"></div>
            <div className="overlay2"></div>
            <div className="textOverlay">
              <h1 className="head head2">OUR PRODUCT</h1>
              <h3 className="subhead">Private Coaching UI Esports</h3>
              <p className="plainText">
              UI Esports membuka jasa private coaching yang terbuka untuk umum dengan coach yang berpengalaman di bidangnya. Tidak hanya membahas mengenai teknis permainan, coach dari UI Esports juga dilatih agar dapat mengembangkan mindset dan pola pikir player untuk dapat diaplikasikan dalam kehidupan sehari-hari.
              </p>
            </div>
          </div>
          <div className="imageCarousel">
            <img src="https://storage.googleapis.com/uiesports/assets/landing-page/Thumbnail%20Carousel_1.webp" alt="image3" className="desktop-image"/>
            <img src="https://storage.googleapis.com/uiesports/assets/landing-page/mobile-carousel1.webp" alt="image3" className="mobile-image"/>
            <div className="overlay1"></div>
            <div className="overlay2"></div>
            <div className="textOverlay">
              <h1 className="head head3">OUR PRODUCT</h1>
              <h3 className="subhead">Pre - Order Jersey UI Esports</h3>
              <p className="plainText">
              UI Esports membuka pre-order Jersey yang desainnya modern dengan filosofi Universitas Indonesia serta menggunakan kualitas bahan yang premium. jersey ini juga tersedia dengan custom name tag yang bisa dikustomisasi oleh customer sesuka hati.
              </p>
            </div>
          </div>
        </Carousel>
        <div>
          <div className='topTransition'>
          <img src="/top-transition.webp" />
          </div>

          <div className='teams-container'>
            <ValorantCard />
            <Dota2Card />
            <PubgmCard />
            <MlCard />
          </div>

          <div className='thunderTransition'>
          <img src="/thunder-transition.webp" />
          <ProfileCard />
          </div>
        </div>
        </div>
        <Modal isOpen={modalShow} toggle={() => setModal(modalShow)} className="completion-modal">
          <ModalHeader>Profile Not Completed</ModalHeader>
          <ModalBody>
            Your Account Profile still need a completion. Please fill the less of the information.
          </ModalBody>
          <ModalFooter>
            <Button className="primary-btn" href="/formProfile">Complete Now</Button>{' '}
            <Button className="secondary-btn" onClick={() => setModal(false)}>Later</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }

Index.getLayout = (page) => (
  <Layout>
    {page}
  </Layout>
)
