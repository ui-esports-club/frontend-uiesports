import React from "react";
import Head from "next/head";
import Link from "next/link";
import { Container } from 'reactstrap';
import styled from "styled-components";
import Hero from "../components/Events/Hero";
import Recent from "../components/Events/Recent";
import Featured from "../components/Events/Featured";
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react'
import {IoIosArrowDropleftCircle, IoIosArrowDroprightCircle} from 'react-icons/io'

import 'swiper/css' 

const SwiperButton = ({mode}) => {

    const swiper = useSwiper()

    const SwiperButton = styled.div`
        position: absolute;
        ${mode === 'next' ? 'right:0;' : 'left:0;'}
        top: 40%;
        z-index: 99999999;
        cursor: pointer;

        @media (max-width: 576px) {
            display: none;
        }
        
    `

    return (
        <SwiperButton className={mode} 
        onClick={() => mode === 'next' ? swiper.slideNext() : swiper.slidePrev()}
        >
            { mode === 'next' ? 
                <IoIosArrowDroprightCircle color="white" size={50} /> :
                <IoIosArrowDropleftCircle color="white" size={50} />
            }
        </SwiperButton>
    )
}


const EventsPage = () => {

    const images = [
        "https://storage.googleapis.com/uiesports/assets/events/recent-image1.webp",
        "https://storage.googleapis.com/uiesports/assets/events/recent-image2.webp",
        "https://storage.googleapis.com/uiesports/assets/events/recent-image3.webp",
        "https://storage.googleapis.com/uiesports/assets/events/recent-image1.webp",
        "https://storage.googleapis.com/uiesports/assets/events/recent-image2.webp",
        "https://storage.googleapis.com/uiesports/assets/events/recent-image3.webp",
    ]

    return (
        <div style={{backgroundColor: "#1E2021", paddingBottom:"200px"}}>
            <Head>
                <title>Events - UI Esports Club</title>
            </Head>
            <Hero bgImage="https://storage.googleapis.com/uiesports/assets/events/HeroImage.webp">
                <Hero.Title>
                    EVENTS
                </Hero.Title>
                <Hero.Subtitle>
                    Explore UI Esports' fantastic events
                </Hero.Subtitle>
            </Hero>

            <Featured>
                <Container>
                    <h2 className="featured-title">Featured Events</h2>

                    {/* Card 1 */}
                    <Featured.Card
                        style={{marginTop: '46px'}}
                        bgColor={"linear-gradient(100.05deg, #5E3580 0%, #FF00D6 100%);"}
                    >
                        <div className="card-title-wrapper">
                            <Featured.CardTitle> 
                                UI ESPORTS CUP 2022
                            </Featured.CardTitle>
                            <Featured.CardSubtitle style={{maxWidth: '640px'}}>
                                Biggest Esports Competition in Universitas Indonesia which competes for six games
                            </Featured.CardSubtitle>
                        </div>
                        <div className="card-image-wrapper">
                            <img src="https://storage.googleapis.com/uiesports/assets/events/CardImage1-crop-new.webp" />
                        </div>
                    </Featured.Card>

                    {/* Card 2 */}
                    <Featured.Card 
                        style={{marginTop: '70px'}} 
                        bgColor={"linear-gradient(100.05deg, #2A4A70 0%, #B8FF45 100%)"}
                    >
                        <div className="card-title-wrapper">
                            <Featured.CardTitle> 
                                UIESC Talks
                            </Featured.CardTitle>
                            <Featured.CardSubtitle>
                                Let’s improve life skills through your gaming skills
                            </Featured.CardSubtitle>
                        </div>
                        <div className="card-image-wrapper">
                            <img src="https://storage.googleapis.com/uiesports/assets/events/CardImage2-crop-new.webp" />
                        </div>
                    </Featured.Card>
                </Container>  
            </Featured>

            <Recent>  
                <Container className="recent-container">
                    <h2 className="recent-title">
                        Recent Events
                    </h2> 
                    <Swiper
                        spaceBetween={40}
                        slidesPerView={1}
                        breakpoints={{
                            300: {
                                slidesPerView: 1,
                                spaceBetween: 20,
                            },
                            768: {
                                slidesPerView: 2,
                                spaceBetween: 40,
                            },
                            1024: {
                                slidesPerView: 3,
                                spaceBetween: 50
                            }
                        }}
                    >
                        <SwiperButton mode="prev" />
                        <SwiperButton mode="next" />
                        {images.map((item, idx) => {
                            return (
                                <SwiperSlide key={idx}> 
                                    <Recent.Card>
                                        <Recent.CardImage> 
                                            <Recent.CardImageTag 
                                                bgColor="linear-gradient(100.05deg, #2A4A70 0%, #B8FF45 100%);"
                                            >
                                                OPEN REGISTRATION
                                            </Recent.CardImageTag>
                                            <img src={item} />
                                        </Recent.CardImage>
                                        <Recent.CardTitle>
                                            UI Esports Cup
                                        </Recent.CardTitle>
                                        <Recent.CardSubtitle className="mt-2">
                                            UI Esports Cup adalah lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ...
                                        </Recent.CardSubtitle>
                                        <div className="d-flex align-items-center mt-4">
                                            <Recent.CardButton>
                                                Register
                                            </Recent.CardButton>
                                            <Link href="/alumni" passHref>
                                                <Recent.Readmore>
                                                    Read More
                                                </Recent.Readmore>
                                            </Link>
                                        </div>
                                    </Recent.Card>
                                </SwiperSlide>
                            )
                        })}

                    </Swiper>
                </Container>
            </Recent>
        </div>
    )
}


export default EventsPage;