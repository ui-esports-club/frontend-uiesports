import Head from 'next/head'

export default function customError() {
    return (
        <div className="customError-wrapper">
            <div className="customError-container">
                {/*  <img src="customError-background.webp" className="customError-background" /> */ }
                <Head>
                <title> Error Page - UI Esports Club </title>
                </Head>  
                <div className='customError-content'>
                    <img src="customError-hero.webp" className="customError-hero" />
                    <h1 className="customError-title"> Something went wrong </h1>
                    <p className="customError-explain"> We couldn’t find what you searched for. </p>
                    <a href="/" className="customError-btn"> Back to homepage </a>
                </div>
                <div className="customError-footerTransition">
               <br></br>
               <br></br>
               <br></br>    
                </div>  
            </div>    
        </div>
    )
}