import NumberCounter from "../components/NumberCounter.js";
import { Swiper, SwiperSlide } from "swiper/react";
import { Scrollbar } from "swiper";
import "swiper/css";
import "swiper/css/pagination";

export default function Alumni() {
    return(
        <div>
            <Head>
                <title>Alumni - UI Esports Club</title>
            </Head>
            <div className="alumnipage-content">

                <div className="alumni-container-header">
                    <img src="https://storage.googleapis.com/uiesports/assets/alumni/alumni-page-background.webp" />
                    <h1 className="alumni-title-header">INITIATING</h1>
                    <p className="alumni-text-header">UI Esports Club 2020-2021</p>
                </div>

                <div className='alumni-section-1 text-white py-5'>
                    <h1 className="alumni-section-1-title alumni-title-base mb-3"><span className="alumni-section-1-subtitle">Our Story</span> Start From</h1>
                    <div className='alumni-wucg-img-container alumni-padx mb-5'>
                        <img className="alumni-section-1-img" src="https://storage.googleapis.com/uiesports/assets/alumni/top4-wucg-masked.webp" />
                    </div>
                    <h1 className="alumni-section-1-title alumni-title-base mb-3">Since That We Have</h1>
                    <div className="alumni-achievement-number-container  alumni-padx">
                        <div className="alumni-achievement-number-box">
                            <div className="alumni-achievement-item-container ">
                                <NumberCounter end={100} extraElement={'+'} timer={40} className={'alumni-achievement-item-number'}/> 
                                Achievements
                            </div>
                            <div className="alumni-achievement-item-container">
                                <NumberCounter end={3000} extraElement={'+'} timer={2} className={'alumni-achievement-item-number'}/>
                                Atentions
                            </div>
                            <div className="alumni-achievement-item-container ">
                                <NumberCounter end={300} extraElement={'+'} timer={15} className={'alumni-achievement-item-number'}/>
                                Members
                            </div>
                        </div>
               
                    </div>
                </div>

                <img src="/thunder-transition-2.png" />

                <div className="alumni-section-2 text-white py-5 alumni-padx">
                    
                    <div className="alumni-fact-pack pt-5">
                        <div className="alumni-fact-pack-title-left alumni-title-base mb-3">HOW IT START</div>
                        <div className="alumni-fact-pack-data-left ">
                            <div className="alumni-fact-pack-text-left">
                                <div className="alumni-fact-pack-text-bold alumni-bold-text-base mt-0 mb-3">We want to accomodate the environment for Universitas Indonesia’s students so they can expressed their talent in esports. </div>
                                <div className="alumni-light-text-base">We started from a little community consists of group of students with the same goals, to develop esports community in Universitas Indonesia. </div>
                            </div>
                            <div className="alumni-fact-pack-photo">
                                <img src="https://storage.googleapis.com/uiesports/assets/alumni/alumni_2.webp" />
                            </div>
                        </div>
                    </div>

                    <div className="alumni-fact-pack pt-5">
                        <div className="alumni-fact-pack-title-right alumni-title-base mb-3">WHAT WE DO</div>
                        <div className="alumni-fact-pack-data-right ">
                            <div className="alumni-fact-pack-photo">
                                <img src="https://storage.googleapis.com/uiesports/assets/alumni/alumni_3.webp" />
                            </div>
                            <div className="alumni-fact-pack-text-right">
                                <div className="alumni-fact-pack-text-bold alumni-bold-text-base mt-0 mb-3">We hold many events to educate our member not only for the video games concept but also the mindset and the real-life skills.</div>
                                <div className="alumni-light-text-base">It’s to create an environment for Universitas Indonesia Students to express their talent and interest in Esports to the world</div>
                            </div>
                        </div>
                    </div>
                </div>

                

                <div className="alumni-section-3 text-white py-5 ">
                    <div className="alumni-event-container ">
                        <div className="alumni-event-card-left alumni-padx ">
                            <div className="alumni-event-img-container">
                                <img className="alumni-event-img-left" src="https://storage.googleapis.com/uiesports/assets/alumni/alumni_4.webp" />
                            </div>
                            <div className="alumni-event-data">
                                <h1 className='alumni-event-title alumni-title-base'>UI ESPORTS CUP</h1>
                                <p className='alumni-light-text-base'>Biggest esports competition in Universitas Indonesia. Attended by 14 faculties in each branch of the tournament.  This is our initiative to scout as many students as possible accross the University</p>
                            </div>
                        </div>
                        <div className="alumni-event-card-right alumni-padx">
                            <div className="alumni-event-data">
                                <h1 className='alumni-event-title alumni-title-base'>WORKSHOP</h1>
                                <p className='alumni-light-text-base'>Our second initiatives is holding educational events such as webinar, online coaching, and providing elder person for the esports division. The goals is so that the young talents can develop themself. This workshop is one them.</p>
                            </div>
                            <div className="alumni-event-img-container">
                                <img className="alumni-event-img-right" src="https://storage.googleapis.com/uiesports/assets/alumni/alumni_5.webp" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className='alumni-section-4-transition'>
                    <Swiper
                        direction="horizontal"
                        slidesPerView={"auto"}
                        spaceBetween={20}
                        loop={true}
                        scrollbar={{
                            hide: true,
                          }}
                        modules={[Scrollbar]}
                        className="mySwiper"
                    >
                        <SwiperSlide >
                            <video muted autoPlay loop>
                                <source src="https://storage.googleapis.com/uiesports/assets/alumni/video-1.webm" type="video/webm"/>
                            </video>
                        </SwiperSlide>
                        <SwiperSlide>
                            <video muted autoPlay loop>
                                <source src="https://storage.googleapis.com/uiesports/assets/alumni/video-2.webm" type="video/webm"/>
                            </video>
                        </SwiperSlide>
                        <SwiperSlide>
                            <video muted autoPlay loop>
                                <source src="https://storage.googleapis.com/uiesports/assets/alumni/video-3.webm" type="video/webm"/>
                            </video>
                        </SwiperSlide>
                    </Swiper>
                </div>

                <div className='alumni-section-4 text-white alumni-padx'>
                    <div className='alumni-section-4-card'>
                        <h1 className='alumni-section-4-title-left alumni-title-base'>WHO WE ARE</h1>
                        <p className='alumni-section-4-text-left alumni-bold-text-base'>We are a student activity unit that accommodates and channels the interests and talents of Universtas Indonesia Esports students.</p>
                    </div>
                    <div className='alumni-section-4-card'>
                        <h1 className='alumni-section-4-title-right'>“</h1>
                        <p className='alumni-section-4-text-right '>“We believe that esports is not a barrier to achievement, but esports is an achievement in itself and a door for other achievements.”</p>
                    </div>
                </div>

                <div className='alumni-section-5-transition alumni-padx'>
                    <h1 className='alumni-title-base alumni-title-base'>OUR VISION</h1>
                    <p className='alumni-bold-text-base'>Become a student activity unit that accommodates and develops an interest in esports talent for all academicians of the University of Indonesia who are active, sporty, competitive, and contributive in the world of esports both at the university level and at the national level</p>
                </div>

                <div className='alumni-section-5 alumni-padx'>
                    <h1 className='alumni-section-5-title alumni-title-base'>OUR MISSION</h1>
                    <div className="alumni-mission-container text-white">
                        <div className="alumni-mission-card-container">
                            <div className="alumni-mission-card">
                                <h1 className="alumni-mission-title alumni-title-base-2">Mission 1</h1>
                                <p className="alumni-mission-text alumni-bold-text-base">Organize and participate in esports competitions both at the university and national level</p>
                            </div>     
                            <div className="alumni-mission-card">
                                <h1 className="alumni-mission-title alumni-title-base-2">Mission 2</h1>
                                <p className="alumni-mission-text alumni-bold-text-base">Participate in observing and analyzing esports developments both at home and abroad</p>
                            </div>  
                        </div>
                        <div className="alumni-mission-card-container">
                            <div className="alumni-mission-card">
                                <h1 className="alumni-mission-title alumni-title-base-2">Mission 3</h1>
                                <p className="alumni-mission-text alumni-bold-text-base">Create sporting and competitive esports athletes both at the university and international level</p>
                            </div>    
                            <div className="alumni-mission-card">
                                <h1 className="alumni-mission-title alumni-title-base-2">Mission 4</h1>
                                <p className="alumni-mission-text alumni-bold-text-base">Creating UI Esports athletes with a Student-Athlete mentality</p>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div className="footer-transition">
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                </div>

            </div>
        </div>
    )
}