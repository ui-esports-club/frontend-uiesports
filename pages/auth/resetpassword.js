import AuthPageFrame from '../../components/AuthPageFrame';



export default function ResetPassword() {

    const submitEmail = async (e) =>{
        e.preventDefault()
        const email = document.getElementById("email").value
        let response = await fetch("http://esports.ui.ac.id:8000/auth/users/reset_password/",{
            method: 'POST',
            body: JSON.stringify({
                email
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        if(response.ok){
            alert('submit berhasil, silahkan cek email')
        }
        else{
            alert('terjadi kesalahan')
        }
        
    }

    const ResetPasswordForm =()=>{
        return(
          <div className='bg-gray0 text-white1 space-y-2 content-center px-10 py-4 m-0 top-0'>
                  <div>
                      <h1 className="authheading font-bold leading-tight text-3xl">Reset Your Password</h1>
                  </div>
                  <div className="text-xs">
                      <p className="mt-3">Please enter your email address.</p>
                      <p className="mb-7">You will receive a link to create a new password via email.</p>
                  </div>
                  <form>
                      <ul className="space-y-3 mb-4 pl-0">
                          <li className="mb-4">
                              <label className="block text-sm font-light" htmlFor="email">Email</label>
                              <input id="email" type="email" required className="p-2 pl-0 w-full bg-transparent border-b-2 font-semibold" />
                          </li>
                          <li>
                              <button type="submit" onClick={submitEmail} 
                              className="bg-gradient-to-r from-neonpink0 to-neonpink1 authsubmitbutton font-semibold text-center mt-4 p-3 w-full"
                              >Submit</button>
                          </li>
                          <li className="">
                            <div className='remember-me'>
                                <a href="login" className="auth-link text-sm font-medium"> <span className='span-space'>&lt;</span>Back to Login</a>
                            </div>

                          </li>
                      </ul>
                  </form>
              </div>
        )
    }

  return (
    <AuthPageFrame formView={ResetPasswordForm()} />
  )
}


