import AuthPageFrame from '../../components/AuthPageFrame';


export default function NewPassword() {
  return (
    <AuthPageFrame formView={NewPasswordForm()} />
  )
}

const NewPasswordForm = () => {
  return (
    <div className='bg-gray0 text-white1 space-y-2 content-center px-10 py-5 m-0 top-0'>
      <div>
        <h1 className="authheading font-bold leading-tight text-3xl">Create New Password</h1>
      </div>
      <div>
        <p className="text-sm mb-5 mt-3">Enter your new password</p>
      </div>
      <form>
        <ul className="space-y-3">
          <li className="">
            <label className="block text-sm font-light" htmlFor="newpassword">New Password</label>
            <input id="newpassword" type="password" required className="p-2 pl-0 w-full bg-transparent border-b-2 font-semibold" />
          </li>
          <li className="">
            <label className="block text-sm font-light" htmlFor="confirmpassword">Confirm Password</label>
            <input id="confirmpassword" type="password" required className="p-2 pl-0 w-full bg-transparent border-b-2 font-semibold" />
          </li>
          <li>
            <button type="submit" className="authsubmitbutton font-semibold text-center my-5 p-3 w-full">Create Password</button>
          </li>
        </ul>
      </form>
    </div>
  )
}