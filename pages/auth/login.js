import {useRouter} from "next/router";
import AuthPageFrame from '../../components/AuthPageFrame';
import { useState } from "react";

export default function Login() { 
    const [checked, setChecked] = useState(false);
    const router = useRouter()
    // useLoginSessionCheck()

    const checkboxHandler = (e) =>{
        if (e.currentTarget.checked) {
            setChecked(true)
            // alert('checked')
        } 
        else {
            setChecked(false)
            // alert('uncchecked')
        }
    }

    const loginUser = async (e) =>{
        e.preventDefault()
        const username = document.getElementById("username").value
        const password = document.getElementById("password").value
        let response = await fetch("/api/login",{
            method: 'POST',
            body: JSON.stringify({
                username,
                password,
                rememberme:checked
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        if(response.ok){
            const data = await response.json();
            sessionStorage.setItem('fromLogin', true);
            router.push('/');
        }
        else{
            alert('username atau password salah')
        }
        
    }


    const useLoginForm =()=> {
        return (
            <div className='bg-gray0 text-white1 space-y-2 content-center px-10 py-4 m-0 top-0 login-container'>
                <div className='m-0 p-0'>
                    <h1 className="authheading font-bold leading-tight text-3xl m-0 p-0">Log In</h1>
                </div>
                <div>
                    <p className="text-sm mb-5 mt-3">Only staff can log in</p>
                </div>
                <form>
                    <ul className="space-y-3 pl-0">
                        <li className="">
                            <label className="block text-sm font-light" htmlFor="name">Username</label>
                            <input id="username" type="text" required className="p-2 pl-0 w-full bg-transparent border-b-2 font-semibold" />
                        </li>
                        <li className="">
                            <label className="block text-sm font-light" htmlFor="name">Password</label>
                            <input id="password" type="password" required className="p-2 pl-0 w-full bg-transparent border-b-2 font-semibold" />
                        </li>
                        <li >
                        <div className="auth-checkbox-container">
                            <input type="checkbox" id="cbauth" onChange={checkboxHandler} checked={checked}/>
                            <label htmlFor="cbauth" className="text-xs font-medium">Remember Me</label>
                            <div className="forgot-pass-container remember-me"><a href="resetpassword" className="auth-link text-xs font-medium ">Forgot Password?</a></div>
                        </div>
                            
                        </li>
                        <li>
                            <button type="submit" onClick={loginUser} 
                            className="bg-gradient-to-r from-neonpink0 to-neonpink1 authsubmitbutton font-semibold text-center mt-3 p-3 w-full"
                            >Log In</button>
                        </li>
                    </ul>
                </form>
            </div>
        )
    }

    return (
        <AuthPageFrame formView={useLoginForm()} />
    )

}


