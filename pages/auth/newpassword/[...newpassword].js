import AuthPageFrame from '../../../components/AuthPageFrame';
import { useState } from 'react';
import {useRouter} from "next/router";

export default function NewPassword() {
  const [pass, setPass] = useState('');
  const [repass, setRepass] = useState('');

  const router = useRouter();


  const checkPassRepassSimilar = (pass1, pass2) => {
    if (pass1 !== '' || pass2 !== '') {
      if (pass1 === pass2) {
        document.getElementById("newPasswordSubmitButton").disabled = false;
        document.getElementById("newPasswordSubmitButton").className = "bg-gradient-to-r from-neonpink0 to-neonpink1 authsubmitbutton font-semibold text-center mt-4 p-3 w-full";
      }
      else {
        document.getElementById("newPasswordSubmitButton").disabled = true;
        document.getElementById("newPasswordSubmitButton").className = "btn-secondary font-semibold text-center mt-4 p-3 w-full";
      }
    }
  }

  const handlePassChange = (event) => {
    event.preventDefault()
    setPass(event.target.value)
    checkPassRepassSimilar(event.target.value, repass)
  }

  const handleRepassChange = (event) => {
    event.preventDefault()
    setRepass(event.target.value)
    checkPassRepassSimilar(event.target.value, pass)
  }

  const handleSubmitPass = async (e) => {
    e.preventDefault()
    const new_password = document.getElementById("newpassword").value
    const re_new_password = document.getElementById("confirmpassword").value
    const resetInfo = window.location.pathname.slice(18).split('/')
    
    const uid = resetInfo[0]
    const token = resetInfo[1]

    let response = await fetch("http://esports.ui.ac.id:8000/auth/users/reset_password_confirm/", {
      method: 'POST',
      body: JSON.stringify({
        uid,
        token,
        new_password,
        re_new_password,
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    if (response.ok) {
      alert('new password has been created')
      router.push('/auth/login')
    }
    else {
      alert('create new password failed')
    }
  }

  const NewPasswordForm = () => {
    return (
      <div className='bg-gray0 text-white1 space-y-2 content-center px-10 py-4 m-0 top-0'>
        <div>
          <h1 className="authheading font-bold leading-tight text-3xl">Create New Password</h1>
        </div>
        <div>
          <p className="text-sm mb-5 mt-3">Enter your new password</p>
        </div>
        <form onSubmit={handleSubmitPass}>
          <ul className="space-y-3 pl-0">
            <li className="">
              <label className="block text-sm font-light" htmlFor="newpassword" >New Password</label>
              <input id="newpassword" name='newpassword' type="password" onChange={handlePassChange} required className="p-2 pl-0 w-full bg-transparent border-b-2 font-semibold" />
            </li>
            <li className="">
              <label className="block text-sm font-light" htmlFor="confirmpassword" >Confirm Password</label>
              <input id="confirmpassword" name='confirmnewpassword' type="password" onChange={handleRepassChange} required className="p-2 pl-0 w-full bg-transparent border-b-2 font-semibold" />
            </li>
            <li>
              <button type="submit" id="newPasswordSubmitButton" disabled className="btn-secondary font-semibold text-center mt-4 p-3 w-full">Create Password</button>
            </li>
          </ul>
        </form>
      </div>
    )
  }

  return (
    <AuthPageFrame formView={NewPasswordForm()} />
  )
}

