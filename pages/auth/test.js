import { useState , useEffect } from "react";
import { getSessionInfo } from "../../functions/sessionAuth";


export default function TestToken() {
  const [access, setAccess] = useState(null)
  const [refresh, setRefresh] = useState(null)
  const [role, setRole] = useState(null)
  const [referer, setReferer] = useState(null)

  useEffect(() => {
    getSessionInfo(setAccess, setRefresh, setRole, setReferer)
  }, [access,refresh,role,referer])

  return (
    <div>
      <h1>TESTING SECRET INFORMATION</h1>
      <p>access: {access}</p>
      <p>refresh: {refresh}</p>
      <p>role: {role}</p>
      <p>referer: {referer}</p>
    </div>
  )
}
