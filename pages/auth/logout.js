import AuthPageFrame from '../../components/AuthPageFrame';
import {useRouter} from "next/router";

//temporary
export default function Logout() {
    const router = useRouter()

    const logoutUser = e =>{
        e.preventDefault()
        let response = fetch("/api/logout",{
            method: 'POST',
            body: JSON.stringify({
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        router.push('/')
    }

    const cancelLogout = e =>{
        e.preventDefault()
        router.push('/')
    }

    

    const useLoginForm =()=> {
        return (
            <div className='bg-gray0 text-white1 content-center px-10 py-5 m-0 top-0'>
                <div className='m-0 p-0'>
                    <h1 className="authheading font-bold leading-tight text-3xl m-0 p-0">Logout</h1>
                </div>
                <div>
                    <p className="text-sm mb-4 mt-3">Are you sure you want to logout?</p>
                </div>
                <div className="logout-button-container">
                    <button type="submit"  onClick={logoutUser} className="confirm-button">Confirm</button>
                    <button type="submit"  onClick={cancelLogout} className="reject-button">Back</button>
                </div>

                
            </div>
        )
    }

    return (
        <AuthPageFrame formView={useLoginForm()} />
    )
} 