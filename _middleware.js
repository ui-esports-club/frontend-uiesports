import { NextResponse } from "next/server"
import { verifyJWTTokenApi, verifyRefreshTokenApi, checkProfileUncomplete } from './functions/sessionAuth'

export default async function middleware(req) {
    let access_token  = "empty_string";
    let refresh_token  = "empty_string";
    let { role } = req.cookies;
    let { origin } = req.nextUrl;
    let {referer} = req.headers;
    var url = req.nextUrl.pathname;
    let response = NextResponse.next();
    if (req.cookies['access_token']) {
        access_token = req.cookies['access_token'];
    }

    if(req.cookies['refresh_token']){
        refresh_token = req.cookies['refresh_token'];
    }
    
    //test url path without page structure
    if (url === '/botak') {
        if (req.method === 'POST') {
            response = NextResponse.json({ message: 'Hello botak POST' })
            response.cookie("server-key", 'value', {
                path: '/',
                maxAge: 1000 * 60 * 60 * 24,
            });
            response.cookie("server-key2", 'value', {
                path: '/',
                maxAge: 1000 * 60 * 60 * 24,
            });
        }
        else {
            return NextResponse.json({ message: 'Hello botak GET' })
        }
    }

    //handle mozilla bug
    if (url.endsWith("/%3Cno%20source%3E")) {
        url = url.slice(0, -18)
        if (url.length === 0) {
            url = '/'
        }
    }

    //Checking Session =================================================
    let sessionAccess = false;
    let newToken = null;

    const verifyjwt = await verifyJWTTokenApi(access_token);

    if(verifyjwt) {
        sessionAccess = true
    }else {
        if (refresh_token) {
            newToken = await verifyRefreshTokenApi(refresh_token);
            if (newToken) {
                sessionAccess = true
                response.cookie("access_token", newToken.access, {
                    path: '/',
                    maxAge: 1000 * 60 * 60 * 24,
                    sameSite: 'lax',
                    httpOnly: true
                })
                const profile_uncompleted = await checkProfileUncomplete(newToken.access)
                response.cookie("profile_uncompleted", profile_uncompleted,{
                    path: '/',
                    sameSite: 'lax',
                })
            }
        }
    }

    //api middleware =============================================================
    if (url === '/api/login') {
        if (sessionAccess) {
            let opt = {status: 400}
            if(newToken){
                opt = {status: 400,headers:{
                    'Set-Cookie': `access_token=${newToken.access};HttpOnly;Max-Age=86400;SameSite=Lax;Path=/`
                }}
            }
            return new Response('Already Login, Please Logout First', opt)
        } 
    }

    //cannot access login page if session allowed =================================================================
    if (url === '/auth/login') {
        if (sessionAccess) {
            return NextResponse.redirect(origin + '/')
        } 
    }

    //Special Routes =================================================================
    if (url === '/login') {
        return NextResponse.redirect(origin + '/auth/login')
    }
    if (url === '/logout') {
        return NextResponse.redirect(origin + '/auth/logout')
    }
    if (url.startsWith('/auth/newpassword') && url.slice(18).split('/').length !== 2) {
        return NextResponse.rewrite(origin + '/auth/error')
    }

    //Protected Routes ================================================================
    //Staff Permission ================================================================
    if ( // ## Insert Protected Routes Here ###
        url === '/auth/test' ||
        url === '/auth/logout' ||
        url === '/formProfile' ||
        url === '/database'
    ) {    // ### checking session ###
        if (!sessionAccess) {
            return NextResponse.redirect(origin + '/auth/login')
        }
    }
    //Admin Permission ================================================================
    if ( // ## Insert Protected Routes Here ###
        url === '/auth/testadmin'
    ) {    // ### checking session ###
        if (!(sessionAccess && role === 'Administrator')) {
            return NextResponse.redirect(origin + '/auth/login')
        }
    }

    return response
}