import React, {useEffect, useRef, useState} from 'react'

export default function NumberCounter({start=0,end=0,timer=30,extraElement='',className=''}) {
    const [number, setNumber] = useState(null);
    const ref = useRef(start);

    const accumulator = end/200;

    const updateCounterState = () =>{
        if(ref.current < end){
            const result = Math.ceil(ref.current + accumulator)
            if(result > end) return setNumber(end)
            setNumber(result)
            ref.current = result;
        }
        setTimeout(updateCounterState,timer)
    }

    useEffect(() => {
        let isMounted = true
        if(isMounted){updateCounterState()}
        return ()=> isMounted = false
    },[end])

    return (
        <div className={className}>{number}{extraElement}</div>
    )
}