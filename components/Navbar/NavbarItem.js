import { useEffect, useState } from "react"



const NavbarItem = (props) => {
    const [hoverState, changeHoverState] = useState(false)
    const [windowState, changeWindowState] = useState("")

    useEffect(() => {
        changeWindowState(window.location.pathname)
    }, [])
    

    return (
        <div className={`navbar-item ${windowState === props.href ? "active" : ""}`}
            onMouseEnter={() => changeHoverState(true)}
            onMouseLeave={() => changeHoverState(false)}
        >
            <a href={props.href}>
                <span>
                    {props.name}
                </span>
                <div className={`navbar-line ${hoverState ? "expand" : ""}`}>
                </div>
            </a>
        </div>
    )
}

export default NavbarItem