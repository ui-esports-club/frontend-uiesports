import axios from 'axios'
import NavbarItem from "./NavbarItem";
import { useRouter } from "next/router";
import react, {useEffect} from "react";
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';


export default function NavBar(props) {
    const [hamburgerOpen, setOpenHamburger] = react.useState(false);
    const [isLogin, setIsLogin] = react.useState(false);
    const [isDropdownOpen, setDropdownOpen] = react.useState(false);
    const [isLoading, setIsLoading] = react.useState(true);
    const [profileData, setProfileData] = react.useState(null)

    const router = useRouter()

    const navbarItems = [ // Customize navbar items here
        {
            name: 'Home',
            href: '/'
        },
        {
            name: 'Database',
            href: '/database'
        },
        // {
        //     name: 'Events',
        //     href: '/events'
        // },
        {
            name: 'Profile',
            href: '/alumni'
        },
        {
            name: 'Event',
            href: '/event'
        },
        {
            name: 'Alumni',
            href: '/alumni'
        },
    ]

    const loginItems = ['Database']

    let toggleDropdown = () => {
        setDropdownOpen(!isDropdownOpen);
    }

    const getProfileData = async (access_token) => {
        const url = process.env.NEXT_PUBLIC_BACKEND_SERVER
        const config = {
            headers: {
                "Authorization" : `Bearer ${access_token}`
            }
        }

        try {
            const res = await axios.get(url + "/profile/", config)
            setProfileData(res.data)
        } catch {}
    }

    useEffect(() => {
        if(props.token){
            setIsLoading(false);
            setIsLogin(true);
            getProfileData(props.token);

        }else if(!props.token){
            setIsLoading(false);
            setIsLogin(false);
        }
    }, [props]);

    return(
        <div className="navbar-container">
            <div className='navbar-desktop'>
                <div className="image-logo"><img src="/uiesports-icon.webp" alt="uiesports icon"/></div>
                <div className='navbar-list'>
                    {navbarItems.map((obj) => {
                        if(loginItems.includes(obj['name'])){
                            if(isLogin){
                                return <NavbarItem
                                    key={obj.name}
                                    name={obj.name}
                                    href={obj.href}
                                />
                            }
                        }else{
                            return <NavbarItem
                                key={obj.name}
                                name={obj.name}
                                href={obj.href}
                            />
                        }
                    })}
                </div>
                {isLoading ? <div></div>:
                !isLogin? 
                    <a href="/auth/login" className='navbar-login-button'>Login</a>:
                    <Dropdown isOpen={isDropdownOpen} toggle={toggleDropdown}>
                        <DropdownToggle onClick={toggleDropdown} data-toggle="dropdown" tag="span">
                        <div className="navbar-profile-photo">
                            {/* <img src="/profile-icon.webp"/> */}
                            <img src={profileData && (profileData.photo || "https://storage.googleapis.com/uiesports/profile/default-photo.webp")} />
                        </div>
                        </DropdownToggle>
                        <DropdownMenu dark>
                            <DropdownItem className="dropdown-item"><a href="/formProfile">Edit Profile</a></DropdownItem>
                            <DropdownItem className="dropdown-item"><a href="/changePassword">Change Password</a></DropdownItem>
                            <DropdownItem className="dropdown-item"><a href="/logout">Logout</a></DropdownItem>
                        </DropdownMenu>
                    </Dropdown>}

                
            </div>
            <div className='navbar-mobile'>
                {hamburgerOpen? 
                <div className='navbar-list-mobile'>
                {navbarItems.map((obj) => (
                    <div key={obj.name} className={`navbar-item`}><a href={obj.href}> {obj.name} </a></div>
                ))}
                {!isLogin? 
                    <div className="navbar-item">
                        <a href="auth/login">Login</a>
                    </div>: 
                    <div>
                        <div className="navbar-item">
                            <a href="#">Profile</a>
                        </div>
                        <div className="navbar-item">
                            <a href="/logout">Logout</a>
                        </div>
                    </div>}
                <div className="hamburg-logo navbar-item" ><button onClick={handleBurger}><img src="/hamburger-icon-navbar.webp" /></button></div>
                </div>:<div className="hamburg-logo" ><button onClick={handleBurger}><img src="/hamburger-icon-navbar.webp" /></button></div>}
            </div>
        </div>
    )
}