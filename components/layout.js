import { useState, useEffect } from "react";
import {useRouter} from "next/router";
import Footer from "./Footer"
import NavBar from "./Navbar/NavBar"
import NavMobile from "./NavbarMobile/NavbarMobile"
import { Navbar } from "reactstrap";

const Layout = ({children}) => {
    const [windowState, changeWindowState] = useState("");
    const [token, setToken] = useState(false);
    const router = useRouter()

    // Insert page that no need a layout
    let noLayout = ['auth'];

    useEffect(() => {
        changeWindowState(router.pathname.split("/")[1]);
    }, [children]);

    useEffect(() => {
        fetch('/api/get-auth', {
            method: 'POST'})
          .then((res) => res.json())
          .then((data) => {
            if (!data.message){
                // setIsLogin(data.access_token)
                // setIsLoading(false)
                setToken(data.access_token);
                // getProfileData(data.access_token)
            } else{
                router.push('/auth/error')
            }

        })
    }, [children, Navbar, NavMobile])

    return (
        <>
            {noLayout.includes(windowState) ? "": 
            <>
            <NavBar token={token} {...children.props}/>
            <NavMobile token={token} {...children.props}/>
            </>
            }
            <main className="text-white min-h-screen">
                {children}
            </main>
            {noLayout.includes(windowState) ? "": <Footer />}
        </>
    )
}

export default Layout