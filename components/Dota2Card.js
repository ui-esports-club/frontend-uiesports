import {useEffect, useState} from 'react';
import {motion, useAnimation} from 'framer-motion';
import {useInView} from 'react-intersection-observer';

export default function Dota2Card(props) {
    const {ref, inView} = useInView({
        threshold: 1
    });
    const animation = useAnimation();
    const [viewed,setViewed] = useState(false);

    useEffect(()=> {
            if(inView){
                animation.start({
                  y: 0,
                  opacity: 1,
                  transition: {duration: .5, ease: [0.43, 0.13, 0.23, 0.96]}
                }),
                setViewed(true)
              }
              if(!inView && !viewed){
                animation.start({
                  y: '3rem',
                  opacity: -1,
                })
              }
              // eslint-disable-next-line react-hooks/exhaustive-deps
            }, [animation,inView])

    return(
        <motion.div className='dota2-card-container' ref={ref} animate={animation}>
            <div className='left-card-image'>
            <img src="/dota2_landing.webp" />
            </div>

            <div className='right-team-name'>DOTA 2</div>
            
            <div className='card-image-mobile'>
            <img src="/dota2_landing_mobile.webp" />
            </div>
        </motion.div>
    )
}