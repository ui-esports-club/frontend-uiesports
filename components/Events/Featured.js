import styled from 'styled-components';


const Featured = styled.div`
    padding: 85px 0;

    .featured-title {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 700;
        font-size: 2rem;
        line-height: 51px;
        color: white;
    }

    @media (max-width: 960px) {
        padding: 2rem;
        .featured-title {
            font-size: 2rem;
            text-align: center;
        }
    }
`


Featured.Card = styled.div`
    background: ${(props) => props.bgColor};
    display: flex;
    flex-direction: row;

    .card-title-wrapper {
        display: flex;
        justify-content: center;
        flex-direction: column;
        padding: 86px 0px 86px 100px;
        width: 60%;
    }

    .card-image-wrapper {
        display: flex;
        justify-content: end;
        align-items: end;
        width: 40%;
    }

    :hover {
        cursor: pointer;    
    }

    @media (max-width: 960px) {
        flex-direction: column;
        padding: 1rem 1.5rem;
        justify-content: end;
        position: relative;
        padding: 0;
        width: 100%;
        height: max-content;

        .card-title-wrapper {
            margin: 3rem 0 0 0;
            padding: 0 20px;
            text-align: center;
            order: 2;
            position: absolute;
            bottom: 0;
            z-index: 99;
        }

        .card-image-wrapper {
            order: 1;
            justify-content: center;
            margin-top: -25px;
        }

        .card-title-wrapper, .card-image-wrapper {
            width: 100%;
        }
    }
`

Featured.CardTitle = styled.span`
    font-family: 'Anton';
    font-style: normal;
    font-weight: 400;
    font-size: 80px;
    line-height: 120px;
    color: white;

    @media (max-width: 960px) {
        font-size: 2.5rem;
        line-height: 2.5rem;
        margin-bottom: 1rem;
        margin-top: -100px;
        font-size: clamp(2rem, 10vw , 3rem);
    }
`

Featured.CardSubtitle = styled.span`
    font-family: 'Inter';
    font-style: normal;
    font-weight: 400;
    font-size: 24px;
    line-height: 29px;
    color: white;

    @media (max-width: 960px) {
        display: none;
    }
`



export default Featured;