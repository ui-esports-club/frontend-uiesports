import styled from "styled-components";

const Hero = styled.div`
    width: 100%;
    min-height: 447px;
    margin-top: -6rem;
    background-image: ${(props) => `url(${props.bgImage})`};
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`

Hero.Title = styled.h1`
    font-family: 'Anton';
    font-size: 5rem;
    font-weight: 400;
    background: linear-gradient(100.05deg, #5E3580 0%, #FF00D6 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    background-clip: text;
    text-fill-color: transparent;
    margin-top: 5rem;

    @media (max-width: 768px) {
        font-size: 3rem;
    }
`

Hero.Subtitle = styled.h2`
    font-family: 'Inter';
    font-style: normal;
    font-weight: 700;
    font-size: 2rem;
    line-height: 2rem;
    color: white;
    text-align: center;

    @media (max-width: 576px) {
        font-size: 1.5rem;
        padding: 0 1rem;
    }
`

export default Hero;