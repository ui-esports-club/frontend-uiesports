import styled from "styled-components";

const Recent = styled.div`
    .recent-title {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 700;
        font-size: 2rem;
        line-height: 51px;

        background: linear-gradient(100.05deg, #5E3580 0%, #FF00D6 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        background-clip: text;
        text-fill-color: transparent;
    }

    .recent-container {
        position: relative;
    }

    .swiper-slide {
        display: flex;
        justify-content: center;
    }

    @media (max-width: 768px) {
        padding: 2rem;
        .recent-title {
            text-align: center;
        }
    }

`

Recent.Card = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 400px;
`

Recent.CardImage = styled.div`
    position: relative;
`

Recent.CardImageTag = styled.div`
    position: absolute;
    left: 0;
    top:0;
    background: ${props => props.bgColor}
    color: white;
    font-family: 'Inter';
    font-style: normal;
    font-weight: 700;
    font-size: .75rem;
    line-height: 10px;
    z-index: 99;
    padding: 10px;
`

Recent.CardTitle = styled.span`
    font-family: 'Inter';
    font-style: normal;
    font-weight: 700;
    font-size: 2rem;
    line-height: 51px;
    color: white;
`

Recent.CardSubtitle = styled.span`
    font-family: 'Inter';
    font-style: normal;
    font-weight: 700;
    font-size: 1rem;
    line-height: 1.25rem;

    /* Gray 4 */

    color: #BDBDBD;
`
Recent.CardButton = styled.button`
    width: 125px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #172A52;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    color: white;
    padding: 11px 29px;
    font-family: 'Inter';
    font-style: normal;
    font-weight: 700;
    font-size: 1rem;
    line-height: 12px;
    min-height: 45px;
    transition: all 0.3s ease;

    :hover {
        background-color: #0b162b;
    }
`

Recent.Readmore = styled.a`
    margin: 0 0 0 20px;
    color: white;
    cursor: pointer;
    padding: 8px 25px;
    font-family: 'Inter';
    font-style: normal;
    font-weight: 500;
    font-size: 1rem;
    line-height: 12px;

    :hover {
        color: white;
    }

`




export default Recent;