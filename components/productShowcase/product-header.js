import Image from "next/image"
import HeaderBackground from '../../public/showcaseproduct-header-background-empty.png'
import HeaderRectangle from '../../public/showcaseproduct-header-rectangle.png'
import HeaderImg1 from '../../public/showcaseproduct-header-img1.png'
import HeaderImg2 from '../../public/showcaseproduct-header-img2.png'
import HeaderImg3 from '../../public/showcaseproduct-header-img3.png'
import HeaderImg4 from '../../public/showcaseproduct-header-img4.png'
import Jersey from '../../public/showcaseproduct-jersey.png'


export default function ProductHeader() {
    return (
        <>
            <div className="productshowcase-header">
                <div className="productshowcase-header-background">
                    <Image
                        src={HeaderBackground}
                        alt='header background'
                        layout="fill"
                        objectFit='cover'
                    />
                </div>
                <div className="productshowcase-header-text">
                    <h1>PRODUCT<br></br> <span>SHOWCASE</span></h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                    </p>
                </div>
                <div className="productshowcase-header-rectangle">
                    <Image
                        src={HeaderRectangle}
                        alt="rectangle"
                        layout="responsive"
                    />
                </div>
                <div className="productshowcase-header-img-1 productshowcase-header-img">
                    <Image
                        src={HeaderImg1}
                        alt="img 1"
                        layout="responsive"
                    />
                </div>
                <div className="productshowcase-header-img-2 productshowcase-header-img">
                    <Image
                        src={HeaderImg2}
                        alt="mileapp-logo"
                        layout="responsive"
                    />
                </div>
                <div className="productshowcase-header-img-3 productshowcase-header-img">
                    <Image
                        src={HeaderImg3}
                        alt="mileapp-logo"
                        layout="responsive"
                    />
                </div>
                <div className="productshowcase-header-img-4 productshowcase-header-img">
                    <Image
                        src={HeaderImg4}
                        alt="mileapp-logo"
                        layout="responsive"
                    />
                </div>
            </div>

            <div className="productshowcase-header-card-container">
                <div className="productshowcase-header-card">
                    <Image
                        src={Jersey}
                        alt="mileapp-logo"
                        layout="responsive"
                    />
                </div>
                <div className="productshowcase-header-card">
                    <Image
                        src={Jersey}
                        alt="mileapp-logo"
                        layout="responsive"
                    />
                </div>
                <div className="productshowcase-header-card">
                    <Image
                        src={Jersey}
                        alt="mileapp-logo"
                        layout="responsive"
                    />
                </div>
                <div className="productshowcase-header-card">
                    <Image
                        src={Jersey}
                        alt="mileapp-logo"
                        layout="responsive"
                    />
                </div>
            </div>

        </>
    )
}