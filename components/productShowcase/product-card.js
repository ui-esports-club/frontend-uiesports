import dummyProduct from '../../public/showcaseProduct-dummy-card-img.webp'
import { HiArrowNarrowRight } from "react-icons/hi";
import Image from 'next/image';
import Link from 'next/link';

export default function ProductCard(props) {

    const bgColor = props.bgColor ? props.bgColor : 'blue'
    const bgClass = 
        bgColor === 'pink' ? 'productshowcase-card-bg-pink' : 
        bgColor === 'green' ? 'productshowcase-card-bg-green' :
        bgColor === 'purple' ? 'productshowcase-card-bg-purple' :
        "productshowcase-card-bg-blue"

    const colorClass = 
        bgColor === 'pink' ? 'productshowcase-card-text-button-pink' : 
        bgColor === 'green' ? 'productshowcase-card-text-button-green' :
        bgColor === 'purple' ? 'productshowcase-card-text-button-purple' :
        "productshowcase-card-text-button-blue"
    return(
        <div className={"productshowcase-card "+bgClass}>
            <div className='productshowcase-card-text-part'>
                <h3 className='productshowcase-card-text-title'>UI Esport Merchandise “Jersey” </h3>
                <p className='productshowcase-card-text-desc'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                </p>
                <Link href='#dummy'>
                    <button className={'productshowcase-card-text-button '+colorClass}>
                        More Info <span> <HiArrowNarrowRight/></span> 
                    </button>
                </Link>
            </div>
            <div className='productshowcase-card-img-part'>
                <div className='productshowcase-card-img-container'>
                    <Image
                        src={dummyProduct}
                        alt='dummmy product img'
                        layout="responsive"
                        objectFit='cover'
                    />
                </div>
            </div>
        </div>
    )
}