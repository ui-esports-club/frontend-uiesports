export default function Footer(props) {
    return(
        <div>
            <div className="footerTransition">
                <img src="/footer-transition.webp"/>
            </div>
            <div className="footer-container">
            <div className="footer-image-logo"><img src="/uiesports-icon.webp"/></div>
            <div className="footer-content-container">
                <div className="footer-title">UI Esports Club</div>
                <div className="footer-social-media-container">
                    <div className="footer-socialmedia-icon"><a href="#"><img src="/instagram-icon.webp" alt="instagram icon"/></a></div>
                    <div className="footer-socialmedia-icon"><a href="#"><img src="/youtube-icon.webp" alt="youtube icon"/></a></div>
                </div>
                <div className="footer-profile"><a href="#">Our Profile</a></div>
                <div className="footer-events"><a href="#">Events</a></div>
            </div>
        </div>
        </div>
    )
}