import React from "react";
import { Zoom } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";

const Slideshow = () => {
  const images = [
    "/images/images1.jpg",
    "/images/images2.jpg",
    "/images/images3.jpg",
  ];

  // Konfigurasi kustom
  const zoomInProperties = {
    indicators: "true",
    scale: 1.2,
    duration: 5000,
    transitionDuration: 500,
    infinite: "true",

    prevArrow: (
      <div style={{ width: "30px", marginRight: "-30px", cursor: "pointer" }}>
        <svg
          width="52"
          height="51"
          viewBox="0 0 52 51"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect
            width="49"
            height="49"
            transform="translate(2)"
            fill="#4F4F4F"
            fillOpacity="0.5"
          />
          <path
            d="M30.2926 32.7092L22.7231 25.3376L30.2926 18.0154L28.5941 16.3333L19.5898 25.3376L28.5941 34.3419L30.2926 32.7092Z"
            fill="white"
          />
        </svg>
      </div>
    ),

    nextArrow: (
      <div style={{ width: "30px", marginLeft: "-30px", cursor: "pointer" }}>
        <svg
          width="52"
          height="51"
          viewBox="0 0 52 51"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect
            width="49"
            height="49"
            transform="translate(50 51) rotate(-180)"
            fill="#4F4F4F"
            fillOpacity="0.5"
          />
          <g clipPath="url(#clip0_111_205)">
            <path
              d="M21.7073 18.2908L29.2768 25.6624L21.7073 32.9846L23.4059 34.6667L32.4102 25.6624L23.4059 16.6581L21.7073 18.2908Z"
              fill="white"
            />
          </g>
          <defs>
            <clipPath id="clip0_111_205">
              <rect
                width="50.8148"
                height="50.8148"
                fill="white"
                transform="translate(51.2563 51) rotate(-180)"
              />
            </clipPath>
          </defs>
        </svg>
      </div>
    ),
  };

  return(
      <div className="m-10">
          <h1>
              Test Carousel 
          </h1>
          <zoom {...zoomInProperties}>
              {images.map((each,index) =>(
                  <div key={index} className="flex justify-center w-full h-full"> 
                    <img src={each} className="w-3/4 object-cover rounded-lg shadow-xl"></img>
                  </div>
              ))}
          </zoom>
      </div>
  )
};

export default Slideshow;
