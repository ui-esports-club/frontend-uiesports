import axios from 'axios';
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import React, { useState, useEffect } from 'react'
import UIEsportsLogo from '../../public/uiesport-logo.webp'
import logo from '../../public/images/NavbarMobile/logo.png'
import HomeIcon from '../../public/images/NavbarMobile/Home.png'
import LoginIcon from '../../public/images/NavbarMobile/Login.png'
import AlumniIcon from '../../public/images/NavbarMobile/Alumni.png'
import EventsIcon from '../../public/images/NavbarMobile/Events.png'
import LogoutIcon from '../../public/images/NavbarMobile/Logout.png'
import DatabaseIcon from '../../public/images/NavbarMobile/Database.png'

const NavbarMobile = (props) => {

    const router = useRouter()
    const [isLogin, setIsLogin] = useState(false);
    const [isActive, setIsActive] = useState(false);
    const [profileData, setProfileData] = useState(null);
    const [colorChange, setColorChange] = useState(false);
    const [isAlertProfileOn, setIsAlertProfileOn] = useState(false);

    const navItems = [
        {
            title: "Home",
            url: "/",
            icon: HomeIcon
        },
        {
            title: "Database",
            url: "/database",
            icon: DatabaseIcon
        },
        // {
        //     title: "Events",
        //     url: "/events",
        //     icon: EventsIcon
        // },
        {
            title: "Profile",
            url: "/alumni",
            icon: AlumniIcon
        },
        {
            title: "Change Password",
            url: "/changePassword",
            icon: "https://storage.googleapis.com/uiesports/assets/navbar-mobile/key.webp"
        },
        {
            title: "Edit Profile",
            url: "/formProfile",
            icon: "https://storage.googleapis.com/uiesports/assets/navbar-mobile/profile.webp"
        },
    ]

    const loginItems = ['Database', 'Change Password', 'Edit Profile'];

    const changeNavbarColor = () => {
        if (window.scrollY >= 80) {
            setColorChange(true)
        } else {
            setColorChange(false)
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', changeNavbarColor)
    }, [])

    const getProfileData = async (access_token) => {
        const url = process.env.NEXT_PUBLIC_BACKEND_SERVER
        const config = {
            headers: {
                "Authorization" : `Bearer ${access_token}`
            }
        }

        try {
            const res = await axios.get(url + "/profile/", config)
            setProfileData(res.data)
        } catch {
            setProfileData(null)
            setIsLogin(false)
        }
    }

    const getProfileCompletion = async (access_token) => {
        const url = process.env.NEXT_PUBLIC_BACKEND_SERVER
        const config = {
            headers: {
                "Authorization" : `Bearer ${access_token}`
            }
        }

        try {
            const res = await axios.get(url + "/profile/completion", config)
            for (let key in res.data) {
                if (!res.data[key]) {
                    setIsAlertProfileOn(true)
                    return
                }
            }
        } catch {}
    }

    useEffect(() => {
        if(props.token){
            setIsLogin(true);
            getProfileData(props.token);
            getProfileCompletion(props.token)
        }else if(!props.token){
            setIsLogin(false);
        }
    }, [props]);

    return (
        <div className={`nav-mobile sticky-top ${colorChange ? "black" : "transparent"}`}>
            <div className={`hamburger-mobile ${isActive ? "active" : ""}`}
                onClick={() => setIsActive(!isActive)}
            >
                <span className='span1' />
                <span className='span2' />
                <span className='span3' />
            </div>
            <div className='logo-mobile'>
                <Image src={logo} width={70} height={35} priority />
            </div>
            <div className={`nav-menu-mobile ${isActive ? "active" : ""}`}>
                <div className='header-logo d-flex align-items-center'>
                    <Image src={UIEsportsLogo} width={40} height={40}/>
                    <span>UI Esports Club</span>
                </div>
                <div className='nav-item-wrapper'>
                    {navItems.map((item, idx) => {
                        if(loginItems.includes(item['title'])){
                            if(isLogin){
                                return (
                                    <div
                                        onClick={() => {
                                            router.push(item.url)
                                            setIsActive(false)
                                        }}
                                        key={idx} 
                                        className={
                                            `nav-item d-flex align-items-center ${router.pathname === item.url ? 'active' : ""}`
                                        }
                                    >
                                        <Image src={item.icon} width={30} height={30} />
                                        <span className='title'>{item.title}</span>
                                    </div>
                                )
                            }
                        }else{
                            return (
                                <div
                                    onClick={() => {
                                        router.push(item.url)
                                        setIsActive(false)
                                    }}
                                    key={idx} 
                                    className={
                                        `nav-item d-flex align-items-center ${router.pathname === item.url ? 'active' : ""}`
                                    }
                                >
                                    <Image src={item.icon} width={30} height={30} />
                                    <span className='title'>{item.title}</span>
                                </div>
                            )
                        }
                    })}
                </div>

                {
                    isLogin ? profileData &&
                    <div className='bottom-box d-flex flex-column align-items-center p-3 mt-auto'>
                        <div className={`reminder-box d-flex flex-column ${isAlertProfileOn ? "" : "hide"}`}>
                            <div className='reminder-chirps'>Reminder</div>
                            <div className='reminder-text'>
                                Your Account Profile still need a 
                                completion. Please fill the less of 
                                the information.
                            </div>
                            <div className={`reminder-cta d-flex justify-content-end mt-3`}>
                                <button 
                                    onClick={() => setIsAlertProfileOn(false)}
                                    className="fill-later"
                                >
                                    Fill Later
                                </button>
                                <div className='fill-now'>
                                    <Link href="/formProfile">Fill Now</Link>
                                </div>
                            </div>
                        </div>
                        <div className='profile-box d-flex justify-content-between mt-3'>
                            <div 
                                className='d-flex profile-info-wrapper align-items-center' 
                                onClick={
                                    () => {
                                        router.push("/formProfile")
                                        setIsActive(false)
                                    }
                                }
                            >
                                <img className='profile-picture' src={profileData.photo || "https://storage.googleapis.com/uiesports/profile/default-photo.webp"} />
                                <div className='user-info d-flex flex-column text-start justify-content-center'>
                                    <div className='full-name'>{profileData.first_name} {profileData.last_name}</div>
                                    <div className='username'>{profileData.username}</div>
                                </div>
                            </div>
                            <div className='logout-button-wrapper d-flex align-items-center'>
                                <div className='logout-button d-flex align-items-center'>
                                    <a className='d-flex align-items-center' href="/logout">
                                        <Image src={LogoutIcon} width={30} height={30} />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div> : 
                    <div className='login-box d-flex align-items-center'>
                        <Image src={LoginIcon} width={30} height={30} />
                        <Link href="/auth/login">Login</Link>
                    </div>
                }
            </div>
        </div>
    )
}

export default NavbarMobile;