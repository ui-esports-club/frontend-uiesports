import Image from 'next/image'
import box_logo_header from '../public/box-logo-header.webp'

export default function AuthPageFrame(props) {
    return (
    <div className='auth-page bg-center bg-cover grid place-items-center h-screen'>
        <div className='w-10/12 max-w-md lg:max-w-lg space-y-0 '>
            <div className='authbox-header'>
                <Image className='' src={box_logo_header} ></Image>
            </div>
            {props.formView}
      </div>
    </div>
    )
}