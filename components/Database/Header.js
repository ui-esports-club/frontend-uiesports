import Image from "next/image"

const Header = () => {

    return (
        <div className="h-[450px] w-full relative flex flex-col justify-center items-center">
            <Image 
                src="/database/database-background.png" 
                layout="fill"
                alt=""
                draggable={false}
                className="z-0"
            />
            

            <Image
                src="/database/database-title.svg"
                width={500}
                height={200}
                alt=""
                draggable={false}
            />


            <div className="text-xl font-semibold z-10">
                Current UI Esports' active staff and teams
            </div>
        </div>
    )


}

export default Header