import {Select, FormControl, MenuItem, InputLabel} from "@mui/material"

const MemberFilter = (props) => {
    return (
        <div className="flex flex-row mt-10 sm:px-4 px-20">
            <div className="text-xl mr-5 sm: flex items-center">
                Account Role :
            </div>
            <div id="filter-dropdown" className="flex flex-row flex-wrap sm:justify-center text-white ">
                <FormControl>
                    <InputLabel id="input-label-divisi">Divisi</InputLabel>
                    <Select
                        labelId="input-label-divisi"
                        id="demo-simple-select"
                        value={props.activeFilter}
                        onChange={props.changeFilterFunction}
                        label="Divisi"
                    >
                        <MenuItem value="Any">Any</MenuItem>
                        <MenuItem value="DOTA 2">DOTA 2</MenuItem>
                        <MenuItem value="PUBGM">PUBGM</MenuItem>
                        <MenuItem value="VALORANT">VALORANT</MenuItem>
                    </Select>
                </FormControl>
            </div>
        </div>
    )
}

export default MemberFilter