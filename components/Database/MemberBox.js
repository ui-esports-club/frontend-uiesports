import Image from "next/image"
import Skeleton from "@mui/material/Skeleton"

const MemberBox = (props) => {

    const badgeColor = {
        'Events & Community': 'linear-gradient(100.05deg, #04D596 0%, #FF00D6 86.98%)',
        'Mobile Legends': 'linear-gradient(100.05deg, #003676 0%, #77C3EA 100%)',
        'Product': 'linear-gradient(100.05deg, #64199F 42.71%, #00D1FF 100%)',
        'Media': 'linear-gradient(100.05deg, #5E3580 0%, #FF00D6 100%)',
        'Creative Business': 'linear-gradient(100.05deg, #4D8035 22.4%, #3300FF 100%)',
        'PSDMO': 'linear-gradient(100.05deg, #BDAD1D 0%, #B02299 100%)',
        'DOTA 2': 'linear-gradient(100.05deg, #612D2A 0%, #FF0D01 100%)',
        'Valorant': 'linear-gradient(90deg, #210709 0%, #FF4655 100%)',
        'PUBGM': 'linear-gradient(100.05deg, #F58634 0%, #DCCC00 100%)',
    }

    if (props.skeleton) {
        return (
            <div>
                <Skeleton variant="rectangular" width={336} height={300} animation="wave" />
                <div className="mt-4">
                    <Skeleton variant="rectangular" width={290} height={20} animation="wave" />
                </div>
                <div className="mt-2">
                    <Skeleton variant="rectangular" width={250} height={20} animation="wave" />
                </div>
            </div>
        )
    }

    return (
        <div className="bg-[#232323]">
            <div className="relative w-full h-[300px]">
                <Image
                    src={props.photo ? props.photo : "/uiesports-admin.webp"}
                    layout="fill"
                    alt=""
                    draggable={false}
                    className="z-0"
                    objectFit="cover"
                />
            </div>
            <div className="flex flex-col p-3">
                {props.divisi.map((obj) => {
                    return(
                    <div style={{'background':`${badgeColor[obj.nama_divisi]}`, 'width': 'fit-content', 'padding':'1% 5%'}}>
                        {obj.nama_divisi}
                    </div>)
                })}
                <div className="text-xl font-bold">
                    {props.first_name ? props.first_name + " " +  props.last_name: props.username}
                </div>
                    {props.tag}
                <a className="text-sm text-right mt-3 text-white no-underline cursor-pointer" onClick={props.onClickFunction}>
                    Details {">"}
                </a>
            </div>
        </div>
    )
}


export default MemberBox