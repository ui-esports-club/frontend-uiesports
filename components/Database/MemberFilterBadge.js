const MemberFilterBadge = (props) => {
    return (
        <div 
            className="bg-gradient-to-br from-[#5E3580] to-[#FF00D6] text-white0 px-[2px] py-[2px] mx-3 sm:my-2 hover:cursor-pointer" 
            onClick={props.onClick}
        >
            <div className={`px-4 py-1 ${props.active ? "bg-transparent" : "bg-[#1E2021]"}`}>
                {props.name}
            </div>
        </div>
    )
}

export default MemberFilterBadge