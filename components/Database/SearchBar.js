const SearchBar = (props) => {
    return (
        <div className="flex justify-center pt-4">
            <input 
                type="text" 
                className="sm:w-9/10 w-2/3 h-12 text-white text-xl border-solid border-[0.1px] border-[#DAE1E7] bg-transparent px-4"
                placeholder="Search based on name or tag"
                value={props.searchFilter}
                onChange={props.changeSearchFilterFunction}
            />
        </div>
    )
}

export default SearchBar