import Image from "next/image"

const Modal = (props) => {


    return (
        <div className={`${props.show ? "fixed" : "hidden"} flex justify-center items-center h-screen w-screen bg-black/70 left-0 top-0 z-20 `}>
            <div className="fixed w-[800px] h-[400px] bg-gradient-to-br from-[#5E3580] to-[#FF00D6] z-20">
            </div>
            <div 
                className="fixed w-[800px] h-[400px] bg-[#333333] z-30 translate-x-4 -translate-y-4 flex flex-row p-4 main-content"
            >
                <div 
                    className="fixed top-4 right-4 hover:cursor-pointer"
                    onClick={props.handleClose}
                >
                    <Image
                        src="/close-icon.svg"
                        width={25}
                        height={25}
                        alt="close"
                    />
                </div>
                <div className="relative w-2/5">
                    <Image
                        src={props.member.photo ? props.member.photo: "https://storage.googleapis.com/uiesports/profile/default-photo.webp"}
                        layout="fill"
                        alt="profile picture"
                        objectFit="cover"
                    />
                </div>
                <div className="ml-8">
                    <div className="text-4xl font-bold">
                        {props.member.first_name ? props.member.first_name + " " +  props.member.last_name: props.member.username}
                    </div>
                    <div className="my-2">
                        {props.member.divisi.map((divisi) => (
                            `${divisi.nama_divisi} `
                        ))}
                    </div>
                    <div className="flex flex-row">
                        <div className="flex flex-col">
                            <div>
                                Faculty
                            </div>
                            <div>
                                NPM / Generation
                            </div>
                        </div>
                        <div className="flex flex-col pl-3">
                            <div>
                                : {props.member.fakultas ? props.member.fakultas: '-'}
                            </div>
                            <div>
                                : {props.member.npm ? props.member.npm: '-'} / {props.member.angkatan ? props.member.angkatan: '-'}
                            </div>
                        </div>

                    </div>
                    <div className="flex flex-row items-center mt-2">
                        <Image
                            src="/line-icon.webp"
                            width={25}
                            height={25}
                            alt="ig icon"
                        />
                        <div className="ml-3">
                            {props.member.id_line ? props.member.id_line: '-'}
                        </div>
                    </div>
                    <div className="flex flex-row items-center mt-2">
                        <Image
                            src="/instagram-icon.webp"
                            width={25}
                            height={25}
                            alt="ig icon"
                        />
                        <div className="ml-3">
                            {props.member.instagram ? props.member.instagram: '-'}
                        </div>
                    </div>
                    <div className="flex flex-row items-center mt-2">
                        <Image
                            src="/email-icon.webp"
                            width={25}
                            height={25}
                            alt="ig icon"
                        />
                        <div className="ml-3">
                            {props.member.email ? props.member.email: '-'}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}


export default Modal