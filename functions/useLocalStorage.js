
export const setItem = (key,value) =>{
  if (typeof window !== 'undefined') {
    if (localStorage){
      localStorage.setItem(key,value)
    }
  }
}

export const getItem = (key) =>{
  if (typeof window !== 'undefined') {
    if (localStorage){
        const gobs= localStorage.getItem(key)
        return gobs
    }
  }
}

export function removeItem(key){
  if (typeof window !== 'undefined') {
    if (localStorage){
        localStorage.removeItem(key)
    }
  }
}
