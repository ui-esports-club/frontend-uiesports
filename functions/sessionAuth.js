
export const verifyJWTTokenApi = async (token)=>{
    try{
        const response = await fetch('http://esports.ui.ac.id:8000/auth/jwt/verify/', {
            method: 'POST',
            body: JSON.stringify({
                token,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        })
 

        if(!response.ok){
            return false
        }
        return true
    }
    catch (error) {
        return false
    }
}

export const verifyRefreshTokenApi = async (refresh)=>{
    try{
        const response = await fetch('http://esports.ui.ac.id:8000/auth/jwt/refresh/', {
            method: 'POST',
            body: JSON.stringify({
                refresh,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        
        if(!response.ok){
            return false
        }
        const data = await response.json()
        return data
    }
    catch (error) {
        return false
    }
}

export const checkProfileUncomplete = async (token)=>{
    // if TRUE than profile is NOT COMPLETE
    // if FALSE than profile is COMPLETE
    try{
        let response = await fetch("http://esports.ui.ac.id:8000/profile/", {
            method: 'GET',
            headers : {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${token}`
            }
        })

        let profile_uncompleted = false;
        if(response.ok){
            const res_data = await response.json()
            if(!(
                res_data.photo && 
                res_data.id_line &&
                res_data.whatsapp &&
                res_data.email &&
                res_data.tag
            )){
                profile_uncompleted = true;
            }
        }
        return profile_uncompleted;
    }
    catch(error){
        return false
    }
}

export const getSessionInfo = async (setAccess=false, setRefresh=false, setRole=false, setReferer=false)=>{
    try{
        const response = await fetch('/api/get-auth', {
            method: 'POST'
        })
        
        if(!response.ok){
            return false
        }
        const data = await response.json()
        setAccess ? setAccess(data.access_token) : '';
        setRefresh? setRefresh(data.refresh_token): '';
        setRefresh? setRole(data.role): '';
        setRefresh? setReferer(data.referer): '';

        return true
    }
    catch (error) {
        return false
    }
}
